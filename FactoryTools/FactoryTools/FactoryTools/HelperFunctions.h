#ifndef FactoryTools_HelperFunctions_H_
#define FactoryTools_HelperFunctions_H_

#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/Jet.h"

#include<vector>

#include "FactoryTools/strongErrorCheck.h"

#include "AthContainers/AuxElement.h"

#include <boost/algorithm/string.hpp>


// -------------------------------------------------------------------
//  HelperFunctions:
//
//
// -------------------------------------------------------------------


namespace FactoryTools {

	class HelperFunctions
	{

	public:

		HelperFunctions() {};

		template <class T, class U>
		static std::pair<T*,U*> createContainerInTStore(std::string name, xAOD::TStore* store)
		{
			std::pair<T*,U*> outputContainers( new T(SG::VIEW_ELEMENTS) , nullptr);
			outputContainers.first->setStore(outputContainers.second);
			STRONG_CHECK_NO_RETURN(store->record( outputContainers.first  , name ) );
			STRONG_CHECK_NO_RETURN(store->record( outputContainers.second , name+"Aux." ) );
			return outputContainers;
		}

		template <class T>
		static T* grabFromStore(std::string name, xAOD::TStore* store){
			T* container(nullptr);
			STRONG_CHECK_NO_RETURN(store->retrieve(container, name) );
			return container;
		}

		template <class T>
		static const T* grabFromEvent(std::string name, xAOD::TEvent* event){
			const T* container(nullptr);
			STRONG_CHECK_NO_RETURN(event->retrieve(container, name) );
			return container;
		}

		static bool checkForSkip(const xAOD::EventInfo* eventInfo, bool createDecor=true){
			// If the event didn't pass the preselection alg, don't bother doing anything with it...
			if(eventInfo->isAvailable<std::string>("regionName")){
				std::string preselectedRegionName =  eventInfo->auxdecor< std::string >("regionName");
				if( preselectedRegionName == "" ) return true;
			} else { // but maybe there was no preselection alg run, so ignore this check.
				if(createDecor) eventInfo->auxdecor< std::string >("regionName") = "";
				else return true;
			}
			return false;
		}


		static auto trigORFromString(std::vector< std::string > passTrigs, std::string trigString){
			boost::replace_all(trigString, "_OR_", ":");
			std::vector<std::string> trigVect;
			boost::split(trigVect,trigString,boost::is_any_of(":") );
			bool trigDecision = 0;
			for(auto iTrig : trigVect){
			  trigDecision |= std::find(passTrigs.begin(), passTrigs.end(), iTrig ) != passTrigs.end();
			}
			return trigDecision;
		};

		static float getJetAttribute(const xAOD::Jet* jet, xAOD::Jet::AttributeID p){
			float value;
			jet->getAttribute(p,value);
			return value;
		}

		static float getJetAttribute(xAOD::Jet* jet, xAOD::Jet::AttributeID p){
			float value;
			jet->getAttribute(p,value);
			return value;
		}

		static bool stringInVector(std::vector< std::string > const inputVector, std::string name){
			return std::find(inputVector.begin(), inputVector.end(), name) != inputVector.end();
		}

        static std::vector<std::string> parseCSV(std::string inputString){
            std::string tmp_string = inputString;
            std::vector<std::string> out_vec; 
            
            
            while( tmp_string.size() > 0) {
                size_t pos = tmp_string.find_first_of(',');
                if(pos == std::string::npos){
                    pos = tmp_string.size();
                    out_vec.push_back(tmp_string.substr(0, pos));
                    tmp_string.erase(0, pos);
                } else {
                    out_vec.push_back(tmp_string.substr(0, pos));
                    tmp_string.erase(0, pos+1);
                }
            }
            return out_vec;
        }



	private:

	};

}

#endif

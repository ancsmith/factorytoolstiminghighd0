#ifndef REGION_VARIABLE_CALCULATOR_PASSTHROUGH_H
#define REGION_VARIABLE_CALCULATOR_PASSTHROUGH_H
//author : Lawrence Lee
//date   : Sept 2018

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include <map>
#include <iostream>

class RegionVarCalculator_passthrough : public RegionVarCalculator {

public :

private :
  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_passthrough, 1);

};

#endif //REGION_VARIABLE_CALCULATOR_PASSTHROUGH_H

//  LocalWords:  ifndef

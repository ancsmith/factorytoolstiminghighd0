#ifndef FactoryTools_StoreDVObjects_H
#define FactoryTools_StoreDVObjects_H

#include <EventLoop/Algorithm.h>
#include <TH3D.h>
#include <TMatrixT.h>

class StoreDVObjects : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;

  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  const std::string& notSetString() {
    static std::string const notSetString = "notSet";
    return notSetString;
  }

  std::string materialMap_Inner_FileName;
  std::string materialMap_Inner_HistName;
  std::string materialMap_Inner_MatrixName;
  std::string materialMap_Outer_FileName;
  std::string materialMap_Outer_HistName;

  std::string m_dvContainerSuffix = "_fixedExtrapolator";

  TH3S* m_materialMap_Inner=0;
  TH3S* m_materialMap_Outer=0;
  TMatrixT<double>* m_materialMap_Matrix=0;

  float m_rDVMax = 300.0;
  float m_zDVMax = 300.0;
  float m_chisqDof = 5.0;
  float m_distMin = 4.0;
  float m_DVMassMin = 10000.0;
  int   m_DVntrkMin = 5;

  StoreDVObjects ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(StoreDVObjects, 1);
};

#endif

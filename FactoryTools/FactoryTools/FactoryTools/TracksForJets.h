#ifndef FactoryTools_TracksForJets_H
#define FactoryTools_TracksForJets_H

#include <EventLoop/Algorithm.h>

class TracksForJets : public EL::Algorithm
{

public:

  std::string NewContainerName = "TracksForJets";

public:

  // this is a standard constructor
  TracksForJets ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(TracksForJets, 1);

  bool onlyPV; // Limit only to PV (ignore pileup vertices)
  float track_d0Sel; // Selection on d0 of input tracks [mm]
  bool do_z0sigCut; // Require track z0 distance significance < z0sig_CutVal w.r.t. PV z-position
  float value_z0sigCut; // See above
  bool do_z0sinThetaCut; // Require |dz0*sin(theta)| < z0sinTheta_CutVal w.r.t. PV z-position
  float value_z0sinThetaCut; // [mm]. See above
  bool do_secondPass; // Only applicable when onlyPV=false. Perform second pass through tracks, attaching each one to closest PV, if that PV z-position is within secondPass_Cut of track z0
  float value_secondPassCut; // [mm]. See above.

};

#endif

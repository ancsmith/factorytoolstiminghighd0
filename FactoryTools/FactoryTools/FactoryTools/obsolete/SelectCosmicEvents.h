#ifndef FactoryTools_SelectCosmicEvents_H
#define FactoryTools_SelectCosmicEvents_H

#include <EventLoop/Algorithm.h>
//asg 
#include "AsgTools/ToolHandle.h"
#include <AsgTools/AnaToolHandle.h>
//muons
#include "MuonSelectorTools/MuonSelectionTool.h"
//triggers
#include "TrigConfxAOD/xAODConfigTool.h"
#include "TrigDecisionTool/TrigDecisionTool.h"
#include "TriggerMatchingTool/MatchingTool.h"

namespace Trig {
  class TrigDecisionTool;
  class IMatchingTool;
}

namespace CP {
  class IMuonSelectionTool;
}

class SelectCosmicEvents : public EL::Algorithm
{
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.
public:
  // float cutValue;



  // variables that don't get filled at submission time should be
  // protected from being send from the submission node to the worker
  // node (done by the //!)
public:
  // Tree *myTree; //!
  // TH1 *myHist; //!

  
  float dR = 0.1; //!
  asg::AnaToolHandle<CP::IMuonSelectionTool>    m_muonSelectionTool; //!

  asg::AnaToolHandle<TrigConf::ITrigConfigTool> m_trigConfTool; //!
  asg::AnaToolHandle<Trig::TrigDecisionTool>    m_trigDecTool; //!
  asg::AnaToolHandle<Trig::IMatchingTool>       m_trigMatchingTool; //!

  // this is a standard constructor
  SelectCosmicEvents ();

  // these are the functions inherited from Algorithm
  virtual EL::StatusCode setupJob (EL::Job& job);
  virtual EL::StatusCode fileExecute ();
  virtual EL::StatusCode histInitialize ();
  virtual EL::StatusCode changeInput (bool firstFile);
  virtual EL::StatusCode initialize ();
  virtual EL::StatusCode execute ();
  virtual EL::StatusCode postExecute ();
  virtual EL::StatusCode finalize ();
  virtual EL::StatusCode histFinalize ();

  // this is needed to distribute the algorithm to the workers
  ClassDef(SelectCosmicEvents, 1);
};

#endif

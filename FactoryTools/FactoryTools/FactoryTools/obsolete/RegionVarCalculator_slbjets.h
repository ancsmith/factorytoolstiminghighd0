#ifndef REGION_VARIABLE_CALCULATOR_SLBJETS_H
#define REGION_VARIABLE_CALCULATOR_SLBJETS_H
//author : Lawrence Lee
//date   : July 2018

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include <map>
#include <iostream>

class RegionVarCalculator_slbjets : public RegionVarCalculator {

public :

private :
  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doSR0LCalculations (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doSR2LCalculations (std::map<std::string, anytype>& /* vars */ );

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_slbjets, 1);
  float m_id_d0_min = -1;
  float m_id_pt_min = 1;
  int m_id_prescale = 100;
  bool m_saveIDtracks = true;
  bool m_saveSegments = false;

};

#endif //REGION_VARIABLE_CALCULATOR_SLBJETS_H

//  LocalWords:  ifndef

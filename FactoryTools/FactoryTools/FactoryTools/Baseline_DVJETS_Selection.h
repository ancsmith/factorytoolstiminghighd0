/*###############################################
 name: Baseline_DVJETS_Selection 
 type: C++ namespace header
 responsible: rcarney@lbl.gov
 
 description:
    Contains a struct, FilterFlags, used to store
    DRAWFilter decisions. Contains flag-setting
    functions and filter logic functions. 

 last updated: May 2018
###############################################*/

#ifndef BASELINE_DVJETS_h
#define BASELINE_DVJETS_h

#include <xAODJet/JetContainer.h>
#include "EventLoop/StatusCode.h"

#include <TH1F.h>

#include <iostream>
#include <vector>
#include <algorithm>    // std::all_of

namespace Baseline_DVJETS_Selection{

        //Bit field for filter decisions
        struct Flags{
            
            //Top level
            unsigned char pass_jetHighPtSelection : 1;
            unsigned char pass_jetLowPtSelection : 1;
            unsigned char pass_jetTracklessSelection : 1;
            unsigned char pass_SRTracklessSelection : 1;
            
            //High-pt jet multiplicity
            unsigned char pass_2_jetHighPtFlag : 1;
            unsigned char pass_3_jetHighPtFlag : 1;
            unsigned char pass_2AND3_jetHighPtFlag : 1;
            unsigned char pass_4_jetHighPtFlag : 1;
            unsigned char pass_5_jetHighPtFlag : 1;
            unsigned char pass_6_jetHighPtFlag : 1;
            unsigned char pass_7_jetHighPtFlag : 1;

            //Low-pt jet multiplicity
            unsigned char pass_2_jetLowPtFlag : 1;
            unsigned char pass_3_jetLowPtFlag : 1;
            unsigned char pass_2AND3_jetLowPtFlag : 1;
            unsigned char pass_4_jetLowPtFlag : 1;
            unsigned char pass_5_jetLowPtFlag : 1;
            unsigned char pass_6_jetLowPtFlag : 1;
            unsigned char pass_7_jetLowPtFlag : 1;

            unsigned char pass_SingleTracklessFlag : 1;
            unsigned char pass_DoubleTracklessFlag : 1;

            //Default value is 0
            Flags(): 
                pass_jetHighPtSelection(0),
                pass_jetLowPtSelection(0),
                pass_jetTracklessSelection(0),
                pass_SRTracklessSelection(0),
                
                pass_2_jetHighPtFlag(0),
                pass_3_jetHighPtFlag(0),
                pass_2AND3_jetHighPtFlag(0),
                pass_4_jetHighPtFlag(0),
                pass_5_jetHighPtFlag(0),
                pass_6_jetHighPtFlag(0),
                pass_7_jetHighPtFlag(0),

                pass_2_jetLowPtFlag(0),
                pass_3_jetLowPtFlag(0),
                pass_2AND3_jetLowPtFlag(0),
                pass_4_jetLowPtFlag(0),
                pass_5_jetLowPtFlag(0),
                pass_6_jetLowPtFlag(0),
                pass_7_jetLowPtFlag(0),

                pass_SingleTracklessFlag(0),
                pass_DoubleTracklessFlag(0){}
                
            //Reset bits to 0
            void reset(){
                pass_jetHighPtSelection = 0;
                pass_jetLowPtSelection = 0;
                pass_jetTracklessSelection = 0;
                pass_SRTracklessSelection = 0;
            
                pass_2_jetHighPtFlag = 0;
                pass_3_jetHighPtFlag = 0;
                pass_2AND3_jetHighPtFlag = 0;
                pass_4_jetHighPtFlag = 0;
                pass_5_jetHighPtFlag = 0;
                pass_6_jetHighPtFlag = 0;
                pass_7_jetHighPtFlag = 0;

                pass_2_jetLowPtFlag = 0;
                pass_3_jetLowPtFlag = 0;
                pass_2AND3_jetLowPtFlag = 0;
                pass_4_jetLowPtFlag = 0;
                pass_5_jetLowPtFlag = 0;
                pass_6_jetLowPtFlag = 0;
                pass_7_jetLowPtFlag = 0;

                pass_SingleTracklessFlag = 0;
                pass_DoubleTracklessFlag = 0;
            }
        };
            
        bool DV_PtFlags( float jetPt, float threshold );

        bool DV_TracklessFlags( float jetPt, float threshold, float jetEta, float sumTrkPt );

        //Use functions above to set flags
        void setFlags( const xAOD::JetContainer* STCalibJets, Flags& flags );
        
        //Filter logic
        EL::StatusCode filterEvents( const xAOD::JetContainer* STCalibJets, Flags& flags );


}
#endif


#ifndef VsiAnalysis_VsiAnaModular_H
#define VsiAnalysis_VsiAnaModular_H

#include <FactoryTools/ModularAlgorithmBase.h>

class VsiAnaModular : public ModularAlgorithmBase
{
 private:

  virtual void initProcessors();

 public:
  // put your configuration variables here as public variables.
  // that way they can be set directly from CINT and python.

  enum class Release { r20p7, r21 };

  int         prop_do_eventInfo;
  int         prop_do_truth;
  int         prop_do_primaryVertices;
  int         prop_do_IDTracksPrescaled;
  int         prop_do_IDTracksSelected;
  int         prop_do_vsiVertices;
  int         prop_do_selectRegion;

  std::string prop_probeTruth;
  std::string prop_containerName;
  std::string prop_vtx_suffix;

  Release     prop_release;
  double      prop_chi2Cut;
  int         prop_hitPatternReq;
  int         prop_doDropAssociated;
  int         prop_fillTracks;
  double      prop_trackStorePrescale;
  double      prop_d0_wrtSVCut;
  double      prop_z0_wrtSVCut;
  double      prop_errd0_wrtSVCut;
  double      prop_errz0_wrtSVCut;
  double      prop_d0signif_wrtSVCut;
  double      prop_z0signif_wrtSVCut;
  double      prop_chi2_toSVCut;


  // this is a standard constructor
  VsiAnaModular();
  ~VsiAnaModular(){};

  // this is needed to distribute the algorithm to the workers
  ClassDef(VsiAnaModular, 1);
};


#endif

#ifndef REGION_VARIABLE_CALCULATOR_JPSI_H
#define REGION_VARIABLE_CALCULATOR_JPSI_H

#include "EventLoop/StatusCode.h"
#include "FactoryTools/RegionVarCalculator.h"

#include "AssociationUtils/OverlapLinkHelper.h"
#include <AnaAlgorithm/AnaAlgorithm.h>

#include <AsgTools/AnaToolHandle.h>
#include <IsolationSelection/IIsolationSelectionTool.h>
#include "PMGAnalysisInterfaces/IPMGCrossSectionTool.h"
#include "PMGTools/PMGCrossSectionTool.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTracking/Vertex.h"
#include "Math/Vector3D.h"



#include <iostream>
#include <map>
#include <string>
#include <vector>

class RegionVarCalculator_jpsi : public RegionVarCalculator {

public :
    RegionVarCalculator_jpsi();

private :

  //todo probably clean this up
  EL::StatusCode doInitialize(EL::IWorker * worker);
  EL::StatusCode doCalculate        (std::map<std::string, anytype>& /* vars */ );
  EL::StatusCode doAllCalculations  (std::map<std::string, anytype>& /* vars */ );

public :
  // this is needed to distribute the algorithm to the workers
  ClassDef(RegionVarCalculator_jpsi, 1);
  std::unique_ptr<ORUtils::OverlapLinkHelper> m_overlapLinkHelper; //!

  //dummy values
  //TODO FIX properly
  double dummy_d         = -999;
  unsigned long dummy_ul = -999;
  float dummy_f          = -999;
  int dummy_i            = -999;


  std::string m_PMGToolFiles = "$FactoryToolsWrapper_DIR/data/FactoryTools/MediumD0/PMGTools";

  // IsolationTool
  asg::AnaToolHandle<CP::IIsolationSelectionTool> m_iso; //!
  asg::AnaToolHandle<PMGTools::IPMGCrossSectionTool> m_PMGCrossSectionTool; //!

};

#endif //REGION_VARIABLE_CALCULATOR_JPSI_H

//  LocalWords:  ifndef

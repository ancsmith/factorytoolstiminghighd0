
import ROOT, glob
import os

def setupEnvironment():

	# CI
	if os.path.exists(os.environ["PWD"]+"/ci_test"):
		print("Assuming this is the CI!")
		os.environ["buildDir"] = os.environ["PWD"]+"/ci_build/"
		os.environ["runDir"]   = os.environ["PWD"]+"/ci_test/"
	# RECAST
	elif os.path.exists("/home/atlas/release_setup.sh"):
		print("Assuming this is RECAST-like!")
		os.environ["buildDir"] = os.environ["PWD"]+"/../../build/"
		os.environ["runDir"]   = os.environ["PWD"]+"/../../run/"
	# Local
	else:
		print("Running locally")
		os.environ["buildDir"] = os.environ["FT"]+"/../../build/"
		os.environ["runDir"]   = os.environ["FT"]+"/../../run/"
	return


def printAndWrite(myString,myFile):

	print(myString)
	myFile.write(myString+"\n")
	return

def printSummary(inputFile,treeName,cuts,inputDSName="",scriptName="test"):

	f    = ROOT.TFile(inputFile)
	t    = f.Get(treeName)
	hist = f.Get("MetaData_EventCount")

	outputFileName = scriptName + "_yields.txt"

	print("\n")
	print(">>> Writing our yields for benchmark cuts to %s\n\n"%outputFileName)

	outputFile = open(outputFileName, "w")

	printAndWrite( scriptName , outputFile)
	printAndWrite( inputDSName , outputFile)

	printAndWrite( "{: >30} : {:}   ".format("N Events"      , hist.GetBinContent(1) ) , outputFile )
	printAndWrite( "{: >30} : {:.2f}".format("Sum of Weights", hist.GetBinContent(3) ) , outputFile )

	for cut in cuts:
		printAndWrite( "{: >30} : {:d}".format(cut, t.Draw("",cut)) , outputFile )

	outputFile.close()

	print("\n")

	return

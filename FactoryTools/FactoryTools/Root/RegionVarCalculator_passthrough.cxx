#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"

#include "FactoryTools/RegionVarCalculator_passthrough.h"
#include "FactoryTools/strongErrorCheck.h"

#include <xAODAnaHelpers/HelperFunctions.h>


// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_passthrough)

EL::StatusCode RegionVarCalculator_passthrough::doInitialize(EL::IWorker * worker) {
	if(m_worker != nullptr){
		std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
		return EL::StatusCode::FAILURE;
	}
	m_worker = worker;

	return EL::StatusCode::SUCCESS;
}

EL::StatusCode RegionVarCalculator_passthrough::doCalculate(std::map<std::string, anytype>& vars) {
		//xAOD::TStore * store = m_worker->xaodStore();//grab the store from the worker
	xAOD::TEvent* event = m_worker->xaodEvent();

	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	eventInfo->auxdecor< std::string >("regionName") = std::string("passthrough");
	std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

	if      ( regionName.empty() ) {return EL::StatusCode::SUCCESS;}
	// If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
	else if ( regionName == "passthrough" ) {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS); }
	// other SR definitions can have other trees written out with other functions here

	return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_passthrough::doAllCalculations(std::map<std::string, anytype>& vars ) {
	xAOD::TStore * store = m_worker->xaodStore();
	xAOD::TEvent * event = m_worker->xaodEvent();

	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	STRONG_CHECK(doGeneralCalculations(vars));

	// Get relevant info from the vertex container //////////////////////
	//

	const xAOD::VertexContainer* vertices = nullptr;
	STRONG_CHECK(event->retrieve( vertices, "PrimaryVertices"));
	vars["NPV"] = HelperFunctions::countPrimaryVertices(vertices, 2);

	//
	/////////////////////////////////////////////////////////////////////

	auto toGeV = [](double a){return a*.001;};

	xAOD::MissingETContainer * metcont = nullptr;
	STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));

	//  std::cout << "MET : " << (*metcont)["Final"]->met() << std::endl;
	vars["passMETtrigger"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
	vars["MET"] = toGeV((*metcont)["Final"]->met());
	vars["MET_phi"] = ((*metcont)["Final"]->phi());


	// retrieve MET_LocHadTopo container
	const xAOD::MissingETContainer* met_lht_container = nullptr;
	STRONG_CHECK( event->retrieve(met_lht_container, "MET_LocHadTopo") );
	bool hasMET = met_lht_container->size() > 0;
	vars["MET_LHT"] = hasMET ? met_lht_container->at(0)->met() * 0.001 : -999.;
	vars["MET_LHT_phi"] = hasMET ? met_lht_container->at(0)->phi() : -999.;

	TVector2 hltMET;
	// retrieve HLT MET
	const xAOD::TrigMissingETContainer* met_hlt_container = nullptr;
	STRONG_CHECK( event->retrieve(met_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET") );
	hasMET = met_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(met_hlt_container->at(0)->ex(), met_hlt_container->at(0)->ey());
	vars["MET_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MET_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

	// retrieve HLT MHT
	const xAOD::TrigMissingETContainer* mht_hlt_container = nullptr;
	STRONG_CHECK( event->retrieve(mht_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht") );
	hasMET = mht_hlt_container->size() > 0;
	if (hasMET) hltMET = TVector2(mht_hlt_container->at(0)->ex(), mht_hlt_container->at(0)->ey());
	vars["MHT_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
	vars["MHT_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

	xAOD::IParticleContainer* jets_nominal(nullptr);
	STRONG_CHECK(store->retrieve(jets_nominal, "STCalibAntiKt4EMTopoJets"));

	for( const auto& jet : *jets_nominal) {
		STRONG_CHECK(addToVectorBranch(vars,"jet_Pt", toGeV(jet->pt() ) ));
		STRONG_CHECK(addToVectorBranch(vars,"jet_Eta", jet->p4().Eta()  ));
		STRONG_CHECK(addToVectorBranch(vars,"jet_Phi", jet->p4().Phi()  ));
		STRONG_CHECK(addToVectorBranch(vars,"jet_M", jet->p4().M()  ));
		STRONG_CHECK(addToVectorBranch(vars,"jet_BTag", jet->auxdata<char>("bjet") ));
		STRONG_CHECK(addToVectorBranch(vars,"jet_PassOR", (int)jet->auxdata<char>("passOR") ));

		if(inListOfDetailedObjects("jets")){

			// // auto* j = dynamic_cast<xAOD::Jet*>( jet );

			// //*******************************
			// // JET CLEANING FLAGS
			// //*******************************
			// //Jet cleaning variables
			// float EMFrac;
			// j->getAttribute(xAOD::JetAttribute::EMFrac,EMFrac);
			// STRONG_CHECK(addToVectorBranch(vars,"calibJet_EMFrac", EMFrac ));

			// float HECFrac = 0;
			// j->getAttribute(xAOD::JetAttribute::HECFrac,HECFrac);
			// STRONG_CHECK(addToVectorBranch(vars,"calibJet_HECFrac", HECFrac ));

			// float LArQuality = 0;
			// j->getAttribute(xAOD::JetAttribute::LArQuality,LArQuality);
			// STRONG_CHECK(addToVectorBranch(vars,"calibJet_LArQuality", LArQuality ));

			// float HECQuality = 0;
			// j->getAttribute(xAOD::JetAttribute::HECQuality,HECQuality);
			// STRONG_CHECK(addToVectorBranch(vars,"calibJet_HECQuality", HECQuality ));

			// float FracSamplingMax = 0;
			// j->getAttribute(xAOD::JetAttribute::FracSamplingMax,FracSamplingMax);
			// STRONG_CHECK(addToVectorBranch(vars,"calibJet_FracSamplingMax", FracSamplingMax ));

			// float NegativeE = 0;
			// j->getAttribute(xAOD::JetAttribute::NegativeE,NegativeE);
			// STRONG_CHECK(addToVectorBranch(vars,"calibJet_NegativeE", NegativeE ));

			// float AverageLArQF = 0;
			// j->getAttribute(xAOD::JetAttribute::AverageLArQF,AverageLArQF);
			// STRONG_CHECK(addToVectorBranch(vars,"calibJet_AverageLArQF", AverageLArQF ));

			// int FracSamplingMaxIndex = -1;
			// j->getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex,FracSamplingMaxIndex);
			// STRONG_CHECK(addToVectorBranch(vars,"calibJet_FracSamplingMaxIndex", FracSamplingMaxIndex ));

			//Run custom jet cleaning to get flags
			// JetCleaning_DVJETS::JetCleaningFlags jetCleaningFlags; //Struct to store jet cleaning decisions
			// JetCleaning_DVJETS::checkCleanJets( *jet, false, true, jetCleaningFlags );

			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail",             jetCleaningFlags.fail));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_jetpT",       jetCleaningFlags.fail_jetpT));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_ugly",        jetCleaningFlags.fail_ugly));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_NCB_lowEta",  jetCleaningFlags.fail_NCB_lowEta));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_NCB_highEta", jetCleaningFlags.fail_NCB_highEta));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_fmaxCut",     jetCleaningFlags.fail_fmaxCut));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_QF",          jetCleaningFlags.fail_QF));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_EMCaloNoise", jetCleaningFlags.fail_EMCaloNoise));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_LLPNegE",     jetCleaningFlags.fail_LLPNegE));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_fmaxMin",     jetCleaningFlags.fail_fmaxMin));
			// STRONG_CHECK(addToVectorBranch(vars,"JetCleanCalib_fail_NCB_monoJet", jetCleaningFlags.fail_NCB_monoJet));
		}

	}

	// Uncalibrated/Trackless jets


	const xAOD::JetContainer* uncalibjets_nominal(nullptr);
	STRONG_CHECK(event->retrieve(uncalibjets_nominal, "AntiKt4EMTopoJets"));

	for (const auto& jet : *uncalibjets_nominal) {
		STRONG_CHECK(addToVectorBranch(vars,"uncalib_JetPt", toGeV(jet->pt() ) ));
		STRONG_CHECK(addToVectorBranch(vars,"uncalib_JetEta", jet->p4().Eta()  ));
		STRONG_CHECK(addToVectorBranch(vars,"uncalib_JetPhi", jet->p4().Phi()  ));
		STRONG_CHECK(addToVectorBranch(vars,"uncalib_JetM", jet->p4().M()  ));
	}

	vars["muSF"] = eventInfo->auxdecor<float>("muSF");

	for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("muSF_systs")) {
			vars["muSF_"+systName+""] = eventInfo->auxdecor<float>("muSF_"+systName);
	}

	vars["elSF"] = eventInfo->auxdecor<float>("elSF");

	for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("elSF_systs")){
			vars["elSF_"+systName+""] = eventInfo->auxdecor<float>("elSF_"+systName);
	}

	double MEff = 0;
	double HT = 0;

	for( const auto& jet : *jets_nominal) {
		HT += toGeV(jet->pt());
	}

	MEff = HT + toGeV((*metcont)["Final"]->met());

	vars["MEff"] = MEff;
	vars["HT"] = HT;

	vars["WZweight"] = eventInfo->auxdecor<float>("WZweight");

	//////////////////////////////////////////////////
	// get the highest-sumpT primary vertex (one is needed for recalculating MET)
	// const xAOD::VertexContainer* pvc = nullptr;
	// STRONG_CHECK( store->retrieve(pvc, "selectedPV") );

	// auto pv = pvc->at(0);
	// vars["PV_x"] = pv->x();
	// vars["PV_y"] = pv->y();
	// vars["PV_z"] =  pv->z();
	// vars["PV_rxy"] =  TMath::Hypot(pv->x(),pv->y());
	// vars["PV_nTracks"] = pv->nTrackParticles();
	// vars["PV_sumpT2"] = pv->auxdataConst<float>( "sumPt2" );
	// // vars["PV_n"] = nPV;

	///////////////////////////////////////////////////////////////
	//to do, write out nominal leptons for SRMET & SR2L
	//xAOD::IParticleContainer* muons_nominal(nullptr);
	//STRONG_CHECK(store->retrieve(muons_nominal, "selectedElectrons"));
	//xAOD::IParticleContainer* electrons_nominal(nullptr);
	//STRONG_CHECK(store->retrieve(electrons_nominal, "selectedMuons"));

	vars["passHLTmsonly"] = eventInfo->auxdecor<bool>("passHLTmsonly");

	//maps to IS & MS tracks
	std::map<int,int> IDtrkToMuonMap;
	std::map<int,int> MStrkToMuonMap;



    xAOD::ElectronContainer* electrons_nominal(nullptr);
    STRONG_CHECK(store->retrieve(electrons_nominal, "STCalibElectrons"));

    for( const auto& lep : *electrons_nominal) {
      STRONG_CHECK(addToVectorBranch( vars, "el_Pt"        ,  toGeV(lep->pt())));
      STRONG_CHECK(addToVectorBranch( vars, "el_Eta"       ,  lep->p4().Eta() ));
      STRONG_CHECK(addToVectorBranch( vars, "el_Phi"       ,  lep->p4().Phi() ));
      // STRONG_CHECK(addToVectorBranch( vars, "el_Sign"      ,  lep->charge() * 11. ));
      STRONG_CHECK(addToVectorBranch( vars, "el_d0"        ,  lep->trackParticle(0)->d0() ));
      STRONG_CHECK(addToVectorBranch( vars, "el_Baseline"  ,  (int)lep->auxdata<char>("baseline") ));
      STRONG_CHECK(addToVectorBranch( vars, "el_Signal"    ,  (int)lep->auxdata<char>("signal") ));
      // STRONG_CHECK(addToVectorBranch( vars, "elPassOR"    ,  (int)lep->auxdata<char>("passOR") ));
    }


	// ID DV mapping :/

	xAOD::IParticleContainer* muons_baseline(nullptr);
	STRONG_CHECK(store->retrieve(muons_baseline, "STCalibMuons"));

	//std::cout << "Looping through selectedBaselineMuons " 	<< std::endl;
	for( xAOD::IParticle * muon : *muons_baseline){

		xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

		STRONG_CHECK(addToVectorBranch( vars, "muon_index"  , mu->index()));
		STRONG_CHECK(addToVectorBranch( vars, "muon_pt"     , toGeV(mu->pt())));
		STRONG_CHECK(addToVectorBranch( vars, "muon_eta"    , mu->p4().Eta()));
		STRONG_CHECK(addToVectorBranch( vars, "muon_phi"    , mu->p4().Phi()));
		STRONG_CHECK(addToVectorBranch( vars, "muon_charge" , mu->charge() ));

	}


	// ///////////////////////////////////////////////////////////////
	// if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){

	// 	// get the truth events to get the true signal vertex position
	// 	// const xAOD::TruthEventContainer* truthEvents = 0;
	// 	// STRONG_CHECK( event->retrieve( truthEvents, "TruthEvents" ));
	// 	// auto trueSignalVertex = *((*truthEvents->begin())->truthVertexLink(0));


	// 	// now get the truth particles
	// 	const xAOD::TruthParticleContainer* truthParticles = 0;
	// 	STRONG_CHECK( event->retrieve( truthParticles, "TruthParticles" ) );
	// 	for (const auto& p : *truthParticles){
	// 		if(p->pdgId()>1e6 && p->hasDecayVtx()) {

	// 			Int_t nCharged = 0;
	// 			Int_t nCharged1GeV = 0;
	// 			TLorentzVector chargedParticleFourVector;
	// 			for(size_t ii = 0; ii < p->decayVtx()->nOutgoingParticles(); ++ii){
	// 				if ( p->decayVtx()->outgoingParticle(ii)->charge() ){
	// 					nCharged++;
	// 					if( toGeV( p->decayVtx()->outgoingParticle(ii)->pt() ) > 1.  ) {
	// 						nCharged1GeV++;
	// 					}
	// 					chargedParticleFourVector +=  p->decayVtx()->outgoingParticle(ii)->p4();
	// 				}
	// 			}
	// 			// float lifetimeLab = (p->decayVtx()->v4().Vect()-trueSignalVertex->v4().Vect()).Mag()/(p->p4().Vect().Mag()/(p->p4().Gamma()*p->p4().M())*TMath::C());

	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_PdgId"               , (int) p->pdgId() ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_Pt"                  , toGeV(p->pt() )  ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_Eta"                 , p->eta()         ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_Phi"                 , p->phi()         ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_M"                   , toGeV(p->m())    ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_BetaGamma"           , p->p4().Beta()*p->p4().Gamma()  ));
	// 			// STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_ProperDecayTime"     , lifetimeLab*p->p4().Gamma()  ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxX"                , p->decayVtx()->x()  ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxY"                , p->decayVtx()->y()  ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxZ"                , p->decayVtx()->z()  ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxNParticles"       , p->decayVtx()->nOutgoingParticles()  ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxNChParticles"     , nCharged  ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxNChParticles1GeV" , nCharged1GeV  ));
	// 			STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxMChParticles"     , toGeV(chargedParticleFourVector.M() )  ));
	// 		}
	// 	}
	// }

	return EL::StatusCode::SUCCESS;
}

// EL::StatusCode RegionVarCalculator_passthrough::doOtherSRCalculations(std::map<std::string, anytype>& /*vars*/ ) {
// 	return EL::StatusCode::SUCCESS;
// }

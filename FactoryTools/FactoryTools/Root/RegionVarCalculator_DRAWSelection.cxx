//=================================
// name: RegionVarCalculator_DRAWSelection
// type: Region-based variable calculator
// responsible: rcarney@lbl.gov
// description:
// 	Places uncalib. jets and
// 	'trackless' jets in StoreGate.
// 	Definition of 'trackless' jets is here:
// 	https://gitlab.cern.ch/atlas/athena/blob/master/PhysicsAnalysis/SUSYPhys/LongLivedParticleDPDMaker/python/DVFlags.py
//
//  last updated: Feb. 2018
//=================================
#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include <xAODTruth/TruthParticleContainer.h>
#include <xAODTruth/TruthVertex.h>

#include "FactoryTools/RegionVarCalculator_DRAWSelection.h"
#include "FactoryTools/strongErrorCheck.h"

#include "TSystem.h"

#include <fstream>
#include <iostream>
#include <xAODAnaHelpers/HelperFunctions.h>

using namespace asg::msgUserCode;
ClassImp(RegionVarCalculator_DRAWSelection)

//*************************
// D O I N I T I A L I Z E
//*************************
EL::StatusCode RegionVarCalculator_DRAWSelection::doInitialize(EL::IWorker * worker) {
	if(m_worker != nullptr){
		std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
		return EL::StatusCode::FAILURE;
	}
	m_worker = worker;

	return EL::StatusCode::SUCCESS;
}

//**************************
// D O   C A L C U L A T E
//**************************
EL::StatusCode RegionVarCalculator_DRAWSelection::doCalculate(std::map<std::string, anytype>& vars )
{
	xAOD::TEvent* event = m_worker->xaodEvent();

	const xAOD::EventInfo* eventInfo = nullptr;
	STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

	std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

	if      ( regionName.empty() ) {
		ANA_MSG_DEBUG("No region name set, no calculations performed.");
		return EL::StatusCode::SUCCESS;
	}

	else if ( regionName == "earlyRun2" ) {
		ANA_MSG_DEBUG("earlyRun2 region set.");
		return EL::StatusCode(  storeDRAWFilterAttributes(vars) == EL::StatusCode::SUCCESS); }

	return EL::StatusCode::SUCCESS;
}

//**************************
// S T O R E  D R A W
//**************************
EL::StatusCode RegionVarCalculator_DRAWSelection::storeDRAWFilterAttributes(std::map<std::string, anytype>& vars )
{
    auto toGeV = [](double a){return a*.001;};

    xAOD::TEvent * event = m_worker->xaodEvent();

    const xAOD::EventInfo* eventInfo = nullptr;
    STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));
    /* TODO: do we want any event info? Most of the relevant
     *  info is stored by doGeneralCalculations in the base class
     */
    STRONG_CHECK(doGeneralCalculations(vars));

    // retrieve MET_LocHadTopo container
    const xAOD::MissingETContainer* met_LocHadTopo_container = nullptr;
	STRONG_CHECK( event->retrieve(met_LocHadTopo_container, "MET_LocHadTopo") );
	bool hasMET = met_LocHadTopo_container->size() > 0;
	vars["MET_LHT"] = hasMET ? met_LocHadTopo_container->at(0)->met() * 0.001 : -999.;
	vars["MET_LHT_phi"] = hasMET ? met_LocHadTopo_container->at(0)->phi() : -999.;

    //isMC flag
    bool isMC = eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION);
    vars["isMC"] = isMC;

    // Trackless jets
    const xAOD::JetContainer* uncalibjets_nominal(nullptr);
    STRONG_CHECK(event->retrieve(uncalibjets_nominal, "AntiKt4EMTopoJets"));

    for (const auto& jet : *uncalibjets_nominal){

        STRONG_CHECK(addToVectorBranch(vars,"uncalibJetPt", toGeV(jet->pt() ) ));
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJetEta", jet->p4().Eta()  ));
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJetPhi", jet->p4().Phi()  ));
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJetM", jet->p4().M()  ));

        //Store relevant info for trackless jets
        std::vector<float> sumPtTrkvec;
        jet->getAttribute(xAOD::JetAttribute::SumPtTrkPt500, sumPtTrkvec);

        //Keep track of if this branch exists to make sure not mixing up entries in flattened branches.
        if( sumPtTrkvec.size() > 0){
            STRONG_CHECK(addToVectorBranch(vars,"SumPtTrkPt500", toGeV(sumPtTrkvec[0]) ));
            STRONG_CHECK(addToVectorBranch(vars,"bool_sumPtTrk", true ));
        } else {
            STRONG_CHECK(addToVectorBranch(vars,"bool_sumPtTrk", false ));
        }

        //Jet cleaning variables
        float EMFrac;
        jet->getAttribute(xAOD::JetAttribute::EMFrac,EMFrac);
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJet_EMFrac", EMFrac ));

        float HECFrac = 0;
        jet->getAttribute(xAOD::JetAttribute::HECFrac,HECFrac);
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJet_HECFrac", HECFrac ));

        float LArQuality = 0;
        jet->getAttribute(xAOD::JetAttribute::LArQuality,LArQuality);
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJet_LArQuality", LArQuality ));

        float HECQuality = 0;
        jet->getAttribute(xAOD::JetAttribute::HECQuality,HECQuality);
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJet_HECQuality", HECQuality ));

        float FracSamplingMax = 0;
        jet->getAttribute(xAOD::JetAttribute::FracSamplingMax,FracSamplingMax);
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJet_FracSamplingMax", FracSamplingMax ));

        float NegativeE = 0;
        jet->getAttribute(xAOD::JetAttribute::NegativeE,NegativeE);
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJet_NegativeE", NegativeE ));

        float AverageLArQF = 0;
        jet->getAttribute(xAOD::JetAttribute::AverageLArQF,AverageLArQF);
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJet_AverageLArQF", AverageLArQF ));

        int FracSamplingMaxIndex = -1;
        jet->getAttribute(xAOD::JetAttribute::FracSamplingMaxIndex,FracSamplingMaxIndex);
        STRONG_CHECK(addToVectorBranch(vars,"uncalibJet_FracSamplingMaxIndex", FracSamplingMaxIndex ));
    }

    //Truth particls
    const xAOD::TruthParticleContainer *truthParticles = nullptr;
    ANA_CHECK( event->retrieve( truthParticles, "TruthParticles"));

    if( truthParticles == 0 ){
        std::cout<<"TruthParticles container is empty"<<std::endl;
        return EL::StatusCode::FAILURE;
    }

    for (auto truthParticle : *truthParticles) {

        if( truthParticle->pdgId() == 1000022 ){ //if neutralino

            //Pythia producs multiple
            int nChildren = truthParticle->nChildren();
            bool notFinalX0 = false;
            for( int i=0; i<nChildren; i++){
                if( truthParticle->child(i)->pdgId() == 1000022 )
                    notFinalX0 = true; //If the X0 does not decay to another X0, it is teh last in the chain and we want to measure it.
            }


            //**********************************************
            //Calculate the neutralino lifetime in its rest frame
            //**********************************************
            if( !notFinalX0 && nChildren>0 ){
                TLorentzVector prodVtx;
                TLorentzVector decayVtx;
                if( truthParticle->hasProdVtx() ){
                    prodVtx = truthParticle->prodVtx()->v4();
                    STRONG_CHECK(addToVectorBranch(vars,"N1_hasProdVtx", 1));
                    STRONG_CHECK(addToVectorBranch(vars,"N1_hasProdVtx_pT", truthParticle->pt()));
                } else{
                    STRONG_CHECK(addToVectorBranch(vars,"N1_noProdVtx", 1));
                    STRONG_CHECK(addToVectorBranch(vars,"N1_noProdVtx_pT", truthParticle->pt()));
                } if( truthParticle->hasDecayVtx() ){
                    STRONG_CHECK(addToVectorBranch(vars,"N1_hasDecayVtx", 1));
                    STRONG_CHECK(addToVectorBranch(vars,"N1_hasDecayVtx_pT", truthParticle->pt()));
                    decayVtx = truthParticle->decayVtx()->v4();
                } else{
                    STRONG_CHECK(addToVectorBranch(vars,"N1_noDecayVtx",1));
                    STRONG_CHECK(addToVectorBranch(vars,"N1_noDecayVtx_pT", truthParticle->pt()));
                }

                //Record lifetime only if both vertices are intact, for now.
                if( truthParticle->hasProdVtx() &&  truthParticle->hasDecayVtx()){
                    TLorentzVector diffVtx = decayVtx - prodVtx; //mm
                    float distanceTravelled = sqrt( diffVtx.X()*diffVtx.X() + diffVtx.Y()*diffVtx.Y() + diffVtx.Z()*diffVtx.Z() );
                    float lifetime = 1e6*distanceTravelled/(TMath::C() * truthParticle->p4().Beta() * truthParticle->p4().Gamma() ); //ns

                    STRONG_CHECK(addToVectorBranch(vars,"neutralino_prodVtx_x", prodVtx.X() ));
                    STRONG_CHECK(addToVectorBranch(vars,"neutralino_prodVtx_y", prodVtx.Y() ));
                    STRONG_CHECK(addToVectorBranch(vars,"neutralino_prodVtx_z", prodVtx.Z() ));
                    STRONG_CHECK(addToVectorBranch(vars,"neutralino_decayVtx_x", decayVtx.X() ));
                    STRONG_CHECK(addToVectorBranch(vars,"neutralino_decayVtx_y", decayVtx.Y() ));
                    STRONG_CHECK(addToVectorBranch(vars,"neutralino_decayVtx_z", decayVtx.Z() ));
                    STRONG_CHECK(addToVectorBranch(vars,"neutralino_lifetime", lifetime));
                }
            }
        }
    }


    return EL::StatusCode::SUCCESS;
}

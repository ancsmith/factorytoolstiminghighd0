#include "EventLoop/Job.h"
#include "EventLoop/StatusCode.h"
#include "EventLoop/IWorker.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include "xAODBase/IParticleContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTrigMissingET/TrigMissingETContainer.h"
#include "xAODTruth/TruthEventContainer.h"


#include "FactoryTools/RegionVarCalculator_highd0.h"
#include "FactoryTools/strongErrorCheck.h"
#include "EventPrimitives/EventPrimitivesHelpers.h"

#include "AsgMessaging/MessageCheck.h"
#include <xAODAnaHelpers/HelperFunctions.h>

// Determines if an id track is back to back with a muon
bool cosmic_Tag_highd0( TLorentzVector p1, TLorentzVector p2){

    float absdphi = fabs(p1.DeltaPhi(p2));  
    float sumeta = p1.Eta()+p2.Eta();
    float dR_cos = sqrt( ( absdphi - TMath::Pi() )*( absdphi - TMath::Pi() ) + sumeta*sumeta );	

    if (dR_cos < 0.1) return true;
    else return false;

}




// this is needed to distribute the algorithm to the workers
ClassImp(RegionVarCalculator_highd0)
using namespace asg::msgUserCode;

    EL::StatusCode RegionVarCalculator_highd0::doInitialize(EL::IWorker * worker) {
        if(m_worker != nullptr){
            std::cout << "You have called " << __PRETTY_FUNCTION__ << " more than once.  Exiting." << std::endl;
            return EL::StatusCode::FAILURE;
        }
        m_worker = worker;

        return EL::StatusCode::SUCCESS;
    }

EL::StatusCode RegionVarCalculator_highd0::doCalculate(std::map<std::string, anytype              >& vars )
{
    //xAOD::TStore * store = m_worker->xaodStore();//grab the store from the worker
    xAOD::TEvent* event = m_worker->xaodEvent();

    const xAOD::EventInfo* eventInfo = nullptr;
    STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

    std::string const & regionName = eventInfo->auxdecor< std::string >("regionName");

    if      ( regionName.empty() ) {return EL::StatusCode::SUCCESS;}
    // If it hasn't been selected in any of the regions from any of the select algs, don't bother calculating anything...
    else if ( regionName == "SR_highd0" ) {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS); }
    else if ( regionName == "SRLEP") {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS &&
            doSR2LCalculations(vars) == EL::StatusCode::SUCCESS);}
    //	else if ( regionName == "SRMET" )  {return EL::StatusCode(doAllCalculations (vars) == EL::StatusCode::SUCCESS &&
    //							 doSR0LCalculations  (vars) == EL::StatusCode::SUCCESS);}


    return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_highd0::doAllCalculations(std::map<std::string, anytype>& vars )
{/*todo*/
    xAOD::TStore * store = m_worker->xaodStore();
    xAOD::TEvent * event = m_worker->xaodEvent();


    const xAOD::EventInfo* eventInfo = nullptr;
    STRONG_CHECK(event->retrieve( eventInfo, "EventInfo"));

    STRONG_CHECK(doGeneralCalculations(vars));

    // Get relevant info from the vertex container //////////////////////
    //

    const xAOD::VertexContainer* vertices = nullptr;
    STRONG_CHECK(event->retrieve( vertices, "PrimaryVertices"));
    vars["NPV"] = HelperFunctions::countPrimaryVertices(vertices, 2);
    auto pv = vertices->at(0);
    double PV_z =  pv->z();
    auto toGeV = [](double a){return a*.001;};

    

    
    /////////////////////////////////////////////////////////////////////
    // MET
    // /////////////////////////////////////////////////////////////////

    xAOD::MissingETContainer * metcont = nullptr;
    STRONG_CHECK(store->retrieve(metcont, "STCalibMET"));

    //  std::cout << "MET : " << (*metcont)["Final"]->met() << std::endl;
    vars["passMETtrigger"] = eventInfo->auxdecor<bool>("isMETTrigPassed");
    vars["MET"] = toGeV((*metcont)["Final"]->met());
    vars["MET_phi"] = ((*metcont)["Final"]->phi());

/*
    // retrieve MET_LocHadTopo container
    const xAOD::MissingETContainer* met_lht_container = nullptr;
    STRONG_CHECK( event->retrieve(met_lht_container, "MET_LocHadTopo") );
    bool hasMET = met_lht_container->size() > 0;
    vars["MET_LHT"] = hasMET ? met_lht_container->at(0)->met() * 0.001 : -999.;
    vars["MET_LHT_phi"] = hasMET ? met_lht_container->at(0)->phi() : -999.;

    TVector2 hltMET;
    // retrieve HLT MET
    const xAOD::TrigMissingETContainer* met_hlt_container = nullptr;
    STRONG_CHECK( event->retrieve(met_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET") );
    hasMET = met_hlt_container->size() > 0;
    if (hasMET) hltMET = TVector2(met_hlt_container->at(0)->ex(), met_hlt_container->at(0)->ey());
    vars["MET_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
    vars["MET_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;

    // retrieve HLT MHT
    const xAOD::TrigMissingETContainer* mht_hlt_container = nullptr;
    STRONG_CHECK( event->retrieve(mht_hlt_container, "HLT_xAOD__TrigMissingETContainer_TrigEFMissingET_mht") );
    hasMET = mht_hlt_container->size() > 0;
    if (hasMET) hltMET = TVector2(mht_hlt_container->at(0)->ex(), mht_hlt_container->at(0)->ey());
    vars["MHT_HLT"] = hasMET ? hltMET.Mod() * 0.001 : -999.;
    vars["MHT_HLT_phi"] = hasMET ? hltMET.Phi() : -999.;
    */

    /////////////////////////////////////////////////////////////////////
    // Jets 
    // /////////////////////////////////////////////////////////////////
    xAOD::IParticleContainer* jets_nominal_unsorted(nullptr);
    STRONG_CHECK(store->retrieve(jets_nominal_unsorted, "selectedJets"));
    std::vector<xAOD::IParticle*> jets_nominal = sortByPt(jets_nominal_unsorted);
    //ATH_MSG_DEBUG("Processing Jets");


    for( const auto& jet : jets_nominal) {
        STRONG_CHECK(addToVectorBranch(vars,"jet_pt", toGeV(jet->pt() ) ));
        STRONG_CHECK(addToVectorBranch(vars,"jet_eta", jet->p4().Eta()  ));
        STRONG_CHECK(addToVectorBranch(vars,"jet_phi", jet->p4().Phi()  ));
        STRONG_CHECK(addToVectorBranch(vars,"jet_M", jet->p4().M()  ));
        STRONG_CHECK(addToVectorBranch(vars,"jet_BTag", jet->auxdata<char>("bjet") ));
        STRONG_CHECK(addToVectorBranch(vars, "jet_passOR" , jet->auxdata<char>("passOR") ? 1 : 0));
    }
    


    vars["muSF"] = eventInfo->auxdecor<float>("muSF");

    for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("muSF_systs")){
        vars["muSF_"+systName+""] = eventInfo->auxdecor<float>("muSF_"+systName);
    }

    vars["elSF"] = eventInfo->auxdecor<float>("elSF");

    for(auto systName : eventInfo->auxdecor< std::vector<std::string> >("elSF_systs")){
        vars["elSF_"+systName+""] = eventInfo->auxdecor<float>("elSF_"+systName);
    }

    //double MEff = 0;
    double HT = 0;

    for( const auto& jet : jets_nominal) {
        HT += toGeV(jet->pt());
    }

    //MEff = HT + toGeV((*metcont)["Final"]->met());

    //vars["MEff"] = MEff;
    vars["HT"] = HT;

    vars["WZweight"] = eventInfo->auxdecor<float>("WZweight");

	//////////////////////////////////////////////////
    // get the highest-sumpT primary vertex (one is needed for recalculating MET)
    /*const xAOD::VertexContainer* pvc = nullptr;
    //STRONG_CHECK( store->retrieve(pvc, "selectedPV") );
    STRONG_CHECK( store->retrieve(pvc, "PrimaryVertices") );

    auto pv = pvc->at(0);
    vars["PV_x"] = pv->x();
    vars["PV_y"] = pv->y();
    vars["PV_z"] =  pv->z();
    vars["PV_rxy"] =  TMath::Hypot(pv->x(),pv->y());
    vars["PV_nTracks"] = pv->nTrackParticles();
    vars["PV_sumpT2"] = pv->auxdataConst<float>( "sumPt2" );
    // vars["PV_n"] = nPV;
    //lesya can't find this in SlepSlep_LLP 
*/

    ///////////////////////////////////////////////////////////////
    // Electrons! 
    // ////////////////////////////////////////////////////////////
    xAOD::IParticleContainer* electrons_baseline_unsorted(nullptr);
    STRONG_CHECK(store->retrieve(electrons_baseline_unsorted, "selectedBaselineElectrons"));
    std::vector<xAOD::IParticle*> electrons_baseline = sortByPt(electrons_baseline_unsorted);

    std::vector<int> LinkedBarcodes;
    LinkedBarcodes.clear();
    int nElectrons = 0;

    //vars["passg140Loose"] = eventInfo->auxdecor<bool>("passg140Loose");
   
    //ATH_MSG_DEBUG("Processing Electrons");

    for( const auto & electron : electrons_baseline){

        xAOD::Electron* el = dynamic_cast<xAOD::Electron*>(electron);

        STRONG_CHECK(addToVectorBranch( vars, "electron_index"  , el->index()));
        STRONG_CHECK(addToVectorBranch( vars, "electron_pt"     , toGeV(el->pt())));
        STRONG_CHECK(addToVectorBranch( vars, "electron_eta"    , el->p4().Eta()));
        STRONG_CHECK(addToVectorBranch( vars, "electron_phi"    , el->p4().Phi()));
        STRONG_CHECK(addToVectorBranch( vars, "electron_charge" , el->charge() ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_passOR" , el->auxdata<char>("passOR") ? 1 : 0));
        STRONG_CHECK(addToVectorBranch( vars, "electron_ptcone20"    , toGeV( el->auxdata< float >("ptcone20")     ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_ptcone30"    , toGeV( el->auxdata< float >("ptcone30")     ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_ptcone40"    , toGeV( el->auxdata< float >("ptcone40")     ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_ptvarcone20" , toGeV( el->auxdata< float >("ptvarcone20")  ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_ptvarcone30" , toGeV( el->auxdata< float >("ptvarcone30")  ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_ptvarcone40" , toGeV( el->auxdata< float >("ptvarcone40")  ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_topoetcone20", toGeV( el->auxdata< float >("topoetcone20") ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_topoetcone30", toGeV( el->auxdata< float >("topoetcone30") ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_topoetcone40", toGeV( el->auxdata< float >("topoetcone40") ) ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_FCLoose",  el->auxdata< float >("FCLoose")  ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_FCTight",  el->auxdata< float >("FCTight")  ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_Gradient",  el->auxdata< float >("Gradient")  ));

        //Offline Timing Tool


        //MC
        double t_coll = 0.0;
	bool corr = false;
	int mode = 0; 
        bool valid = false; double t_corrected = -99999;
        if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){
	    tie(valid, t_corrected) = m_OfflineTimingTool->getSmearedTime(*el, PV_z, t_coll, corr, mode);
            std::cout<<"Just called offline timing tool on electron: "<<valid<<" time: "<< t_corrected<<endl;
	    corr = true;
        }
        else{
	    tie(valid, t_corrected) = m_OfflineTimingTool->getCorrectedTime(*el, false, eventInfo->runNumber(), PV_z);
            std::cout<<"Just called offline time smearing on MC electron: "<<valid<<" time: "<< t_corrected<<endl;
        }


        STRONG_CHECK(addToVectorBranch( vars, "electron_time",  t_corrected  ));
        //STRONG_CHECK(addToVectorBranch( vars, "electron_LHValue",  el->likelihoodValue() ));

        STRONG_CHECK(addToVectorBranch( vars, "electron_nTracks", el->nTrackParticles()));
        for(unsigned int itrk = 0; itrk<el->nTrackParticles(); itrk++){
            const xAOD::TrackParticle* idtrack = el->trackParticle( itrk );
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_index" , idtrack->index() ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_electronindex"  , nElectrons )); //this is the index in the vector
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_pt"    , toGeV(idtrack->pt()) ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_eta"   , idtrack->eta() ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_phi"   , idtrack->phi() ));
            //STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_chi2"        , idtrack->isAvailable<float>("chiSquared") ? idtrack->auxdata< float >("chiSquared")/idtrack->auxdata< float >("numberDoF") : -999)); // no numberDoF available
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_chi2"        , idtrack->isAvailable<float>("chiSquared") ? idtrack->auxdata< float >("chiSquared") : -999)); // chiSquared directly (no DoF)
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_qop"         , idtrack->qOverP() ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_qop_err"     , idtrack->definingParametersCovMatrix()(4,4) / fabs(idtrack->qOverP()) ));

            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_d0"      , idtrack->d0() ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_z0"      , idtrack->z0() ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_isLRT"   , (idtrack->isAvailable<unsigned long>("patternRecoInfo")) ? static_cast<int>(idtrack->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0)) : -999 ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_nPIX"    , idtrack->auxdata< unsigned char >("numberOfPixelHits") ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_nSCT"    , idtrack->auxdata< unsigned char >("numberOfSCTHits")  ));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_nSi"     ,  (unsigned int)idtrack->auxdata< unsigned char >("numberOfSCTHits") + (unsigned int)idtrack->auxdata< unsigned char >("numberOfPixelHits")));
            STRONG_CHECK(addToVectorBranch( vars, "electron_IDtrack_RFirstHit"    , ( idtrack->isAvailable<float>("radiusOfFirstHit") ) ? idtrack->auxdataConst< float >("radiusOfFirstHit") : -999 ));
        }

        STRONG_CHECK(addToVectorBranch( vars, "electron_d0"      , (el->nTrackParticles() > 0) ? el->trackParticle(0)->d0()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_z0"      , (el->nTrackParticles() > 0) ? el->trackParticle(0)->z0()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_trackpt"    , (el->nTrackParticles() > 0) ? toGeV(el->trackParticle(0)->pt()) : -999 ));
        //STRONG_CHECK(addToVectorBranch( vars, "electron_chi2"        , (el->nTrackParticles() > 0 && el->trackParticle(0)->isAvailable<float>("chiSquared")) ? el->trackParticle(0)->auxdata< float >("chiSquared")/el->trackParticle(0)->auxdata< float >("numberDoF")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_chi2"        , (el->nTrackParticles() > 0 && el->trackParticle(0)->isAvailable<float>("chiSquared")) ? el->trackParticle(0)->auxdata< float >("chiSquared") : -999 )); // no numberDoF available
        STRONG_CHECK(addToVectorBranch( vars, "electron_qop"         , (el->nTrackParticles() > 0) ? el->trackParticle(0)->qOverP()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_qop_err"     , (el->nTrackParticles() > 0) ? el->trackParticle(0)->definingParametersCovMatrix()(4,4) / fabs((el->trackParticle(0))->qOverP())  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_nPIX"    , (el->nTrackParticles() > 0) ? el->trackParticle(0)->auxdata< unsigned char >("numberOfPixelHits")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_nSCT"    , (el->nTrackParticles() > 0) ? el->trackParticle(0)->auxdata< unsigned char >("numberOfSCTHits")   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_nTRT"    , (el->nTrackParticles() > 0 && el->trackParticle(0)->isAvailable<unsigned char>("numberOfTRTHits")) ? el->trackParticle(0)->auxdata< unsigned char >("numberOfTRTHits")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_nSi"     ,  (el->nTrackParticles() > 0) ? (unsigned int)(el->trackParticle(0))->auxdata< unsigned char >("numberOfSCTHits") + (unsigned int)(el->trackParticle(0))->auxdata< unsigned char >("numberOfPixelHits") : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "electron_RFirstHit"    , (el->nTrackParticles() > 0 &&  el->trackParticle(0)->isAvailable<float>("radiusOfFirstHit")) ?  el->trackParticle(0)->auxdataConst< float >("radiusOfFirstHit")  : -999 ));
        

        // Shower shape info    
        /*STRONG_CHECK(addToVectorBranch( vars, "electron_e011"    , el->auxdata< float >("e011")      ));
          STRONG_CHECK(addToVectorBranch( vars, "electron_e033"    , el->auxdata< float >("e033")      )); 
          STRONG_CHECK(addToVectorBranch( vars, "electron_e132"    , el->auxdata< float >("e132")      )); 
          STRONG_CHECK(addToVectorBranch( vars, "electron_e1152"   , el->auxdata< float >("e1152")     )); 
          STRONG_CHECK(addToVectorBranch( vars, "electron_ethad1"  , el->auxdata< float >("ethad1")    )); 
          STRONG_CHECK(addToVectorBranch( vars, "electron_ethad"   , el->auxdata< float >("ethad")     )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_ehad1"   , el->auxdata< float >("ehad1")     )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_f1"      , el->auxdata< float >("f1")        )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_f3"      , el->auxdata< float >("f3")        )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_f1core"  , el->auxdata< float >("f1core")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_f3core"  , el->auxdata< float >("f3core")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e233"    , el->auxdata< float >("e233")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e235"    , el->auxdata< float >("e235")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e255"    , el->auxdata< float >("e255")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e237"    , el->auxdata< float >("e237")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e277"    , el->auxdata< float >("e277")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e333"    , el->auxdata< float >("e333")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e335"    , el->auxdata< float >("e335")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e337"    , el->auxdata< float >("e337")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_weta1"   , el->auxdata< float >("weta1")     )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_weta2"   , el->auxdata< float >("weta2")     )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e2ts1"   , el->auxdata< float >("e2ts1")     )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_e2tsts1" , el->auxdata< float >("e2tsts1")   )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_fracs1"  , el->auxdata< float >("fracs1")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_widths1" , el->auxdata< float >("widths1")   )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_widths2" , el->auxdata< float >("widths2")   )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_poscs1"  , el->auxdata< float >("poscs1")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_poscs2"  , el->auxdata< float >("poscs2")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_asy1"    , el->auxdata< float >("asy1")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_pos"     , el->auxdata< float >("pos")       )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_pos7"    , el->auxdata< float >("pos7")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_barys1"  , el->auxdata< float >("barys1")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_wtots1"  , el->auxdata< float >("wtots1")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_emins1"  , el->auxdata< float >("emins1")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_emaxs1"  , el->auxdata< float >("emaxs1")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_r33over37allcalo"    , el->auxdata< float >("r44over37allcalo")     )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_ecore"   , el->auxdata< float >("ecore")     )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_Reta"    , el->auxdata< float >("Reta")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_Rphi"    , el->auxdata< float >("Rphi")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_Eratio"  , el->auxdata< float >("Eratio")    )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_Rhad"    , el->auxdata< float >("Rhad")      )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_Rhad1"   , el->auxdata< float >("Rhad1")     )); 
        STRONG_CHECK(addToVectorBranch( vars, "electron_DeltaE"  , el->auxdata< float >("DeltaE")    )); 
        */

        const ElementLink< xAOD::TruthParticleContainer >& link = el->auxdata< ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");

        if( link.isValid() ){
            const xAOD::TruthParticle* truth = *link;
            STRONG_CHECK(addToVectorBranch( vars, "electron_truthMatchedBarcode"  , truth->barcode()  )); 
            LinkedBarcodes.push_back(truth->barcode());
            
        }
        else
            STRONG_CHECK(addToVectorBranch( vars, "electron_truthMatchedBarcode" , -1                 ));

    nElectrons++;

    }
    
    vars["electron_n"] = nElectrons;


    ////////////////////////////////////////////////////////////////
    // Photons
    // /////////////////////////////////////////////////////////////
    xAOD::IParticleContainer* photons(nullptr);
    STRONG_CHECK(store->retrieve(photons, "selectedPhotons"));
    
    int nPhotons;
    for(xAOD::IParticle* photon : *photons){

        nPhotons++;
        xAOD::Photon* ph = dynamic_cast<xAOD::Photon*>(photon);
        
        STRONG_CHECK(addToVectorBranch( vars, "photon_index"         , ph->index()));
        STRONG_CHECK(addToVectorBranch( vars, "photon_pt"            , toGeV(ph->pt())));
        STRONG_CHECK(addToVectorBranch( vars, "photon_eta"           , ph->p4().Eta()));
        STRONG_CHECK(addToVectorBranch( vars, "photon_phi"           , ph->p4().Phi()));
        STRONG_CHECK(addToVectorBranch( vars, "photon_passOR"        , ph->auxdata<char>("passOR") ? 1 : 0));
        
        STRONG_CHECK(addToVectorBranch( vars, "photon_isConversion"  , xAOD::EgammaHelpers::isConvertedPhoton(ph) ? 1 : 0));
        STRONG_CHECK(addToVectorBranch( vars, "photon_conversionType", (int)ph->conversionType()));
        //STRONG_CHECK(addToVectorBranch( vars, "photon_LHValue"       ,  ph->likelihoodValue() ));

        const ElementLink< xAOD::TruthParticleContainer >& link = ph->auxdata< ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");
        
        if( link.isValid() ){
            const xAOD::TruthParticle* truth = *link;
            STRONG_CHECK(addToVectorBranch( vars, "photon_truthMatchedBarcode"  , truth->barcode()  )); 
            LinkedBarcodes.push_back(truth->barcode());
        }
        else
            STRONG_CHECK(addToVectorBranch( vars, "photon_truthMatchedBarcode" , -1                 ));
    }
    vars["photon_n"] = nPhotons;


    ///////////////////////////////////////////////////////////////////
    // Muons!
    // ////////////////////////////////////////////////////////////////

    //vars["passHLTmsonly"] = eventInfo->auxdecor<bool>("passHLTmsonly");

    //maps to IS & MS tracks
    std::map<int,int> IDtrkToMuonMap;
    std::map<int,int> MStrkToMuonMap;


    xAOD::IParticleContainer* muons_baseline_unsorted(nullptr);
    STRONG_CHECK(store->retrieve(muons_baseline_unsorted, "selectedBaselineMuons"));
    int nMuons = 0;
    std::vector<xAOD::IParticle*> muons_baseline = sortByPt(muons_baseline_unsorted);
    //ATH_MSG_DEBUG("Processing Muons");

    for( const auto & muon : muons_baseline){
        xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);

        STRONG_CHECK(addToVectorBranch( vars, "muon_index"  , mu->index()));
        STRONG_CHECK(addToVectorBranch( vars, "muon_pt"     , toGeV(mu->pt())));
        STRONG_CHECK(addToVectorBranch( vars, "muon_eta"    , mu->p4().Eta()));
        STRONG_CHECK(addToVectorBranch( vars, "muon_phi"    , mu->p4().Phi()));
        STRONG_CHECK(addToVectorBranch( vars, "muon_charge" , mu->charge() ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_passOR" , mu->auxdata<char>("passOR") ? 1 : 0));

        STRONG_CHECK(addToVectorBranch( vars, "muon_ptcone20"    , toGeV( mu->auxdata< float >("ptcone20")     ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_ptcone30"    , toGeV( mu->auxdata< float >("ptcone30")     ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_ptcone40"    , toGeV( mu->auxdata< float >("ptcone40")     ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_ptvarcone20" , toGeV( mu->auxdata< float >("ptvarcone20")  ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_ptvarcone30" , toGeV( mu->auxdata< float >("ptvarcone30")  ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_ptvarcone40" , toGeV( mu->auxdata< float >("ptvarcone40")  ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_topoetcone20", toGeV( mu->auxdata< float >("topoetcone20") ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_topoetcone30", toGeV( mu->auxdata< float >("topoetcone30") ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_topoetcone40", toGeV( mu->auxdata< float >("topoetcone40") ) ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_FCLoose",  mu->auxdata< float >("FCLoose")  ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_FCTight",  mu->auxdata< float >("FCTight")  ));


        STRONG_CHECK(addToVectorBranch( vars, "muon_nPres"      , mu->auxdata< unsigned char >("numberOfPrecisionLayers") ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_nPresGood"  , mu->auxdata< unsigned char >("numberOfGoodPrecisionLayers") ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_nPresHole"  , mu->auxdata< unsigned char >("numberOfPrecisionHoleLayers") ));


        //ID Track  
        const xAOD::TrackParticle* idtrack = mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_hasIDtrack"  , (idtrack) ? 1 : 0 ));

        if (idtrack) {
            IDtrkToMuonMap[ (idtrack)->index() ] = mu->index();
        }
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_index"       ,    (idtrack) ?  idtrack->index()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_muonindex"   ,    (idtrack) ?  mu->index()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_pt"          ,    (idtrack) ?  toGeV(idtrack->pt())  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_eta"         ,    (idtrack) ?  idtrack->eta()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_phi"         ,    (idtrack) ?  idtrack->phi()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_d0"          ,    (idtrack) ?  idtrack->d0()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_z0"          ,    (idtrack) ?  idtrack->z0()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_chi2"        ,    (idtrack && idtrack->isAvailable<float>("chiSquared") ) ?  idtrack->auxdata< float >("chiSquared")/idtrack->auxdata< float >("numberDoF")  : -999 ));
        //STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_chi2"        ,    (idtrack) ?  idtrack->chiSquared()/idtrack->auxdata< float >("numberDoF")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_isLRT"       ,    (idtrack && idtrack->isAvailable<unsigned long>("patternRecoInfo")) ?  static_cast<int>(idtrack->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0))  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_RFirstHit" ,    (idtrack &&  idtrack->isAvailable<float>("radiusOfFirstHit") ) ?  idtrack->auxdata< float >("radiusOfFirstHit" )  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_nPIX"                ,    (idtrack) ?   idtrack->auxdata< unsigned char >("numberOfPixelHits")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_nSCT"                ,    (idtrack) ?   idtrack->auxdata< unsigned char >("numberOfSCTHits")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_nSi"                 ,    (idtrack) ?   (unsigned int)idtrack->auxdata< unsigned char >("numberOfSCTHits") + (unsigned int)idtrack->auxdata< unsigned char >("numberOfPixelHits") : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_nTRT"                ,    (idtrack && idtrack->isAvailable<unsigned char>("numberOfTRTHits")) ?   idtrack->auxdata< unsigned char >("numberOfTRTHits")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_qop"         ,    (idtrack) ?  idtrack->qOverP()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_IDtrack_qop_err"     ,    (idtrack) ?  idtrack->definingParametersCovMatrix()(4,4) / fabs(idtrack->qOverP())  : -999 ));



        const xAOD::TrackParticle* mstrack = mu->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_hasMStrack" , (mstrack) ? 1 : 0 ));
        if (mstrack) {
            MStrkToMuonMap[ (mstrack)->index() ] = mu->index();
            STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_MuonIndex" ,  ( MStrkToMuonMap.find(mstrack->index()) == MStrkToMuonMap.end() ) ? -99 : MStrkToMuonMap[mstrack->index()] ));
        }
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_eta" , (mstrack) ?     mstrack->eta()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_phi" , (mstrack) ?     mstrack->phi()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_pt"  , (mstrack) ?     toGeV( mstrack->pt() )   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_D0"  , (mstrack) ?     mstrack->d0()   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_Z0"  , (mstrack) ?     mstrack->z0()   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_chi2"        , (mstrack && mstrack->isAvailable<float>("chiSquared")) ?  mstrack->auxdata< float >("chiSquared")/mstrack->auxdata< float >("numberDoF")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_ELoss" ,       (mstrack && muon->isAvailable<float>("EnergyLoss")) ?             toGeV(  muon->auxdata<float>("EnergyLoss")   	     )  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_ELossSigma" ,  (mstrack && muon->isAvailable<float>("EnergyLossSigma")) ?        toGeV(  muon->auxdata<float>("EnergyLossSigma") 	     )  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_MeasELoss" ,   (mstrack && muon->isAvailable<float>("MeasEnergyLoss")) ?         toGeV(  muon->auxdata<float>("MeasEnergyLoss") 	     )  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_MeasELossSigma" , (mstrack && muon->isAvailable<float>("MeasEnergyLossSigma")) ? toGeV(  muon->auxdata<float>("MeasEnergyLossSigma")      )  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_ParamELoss" ,  (mstrack && muon->isAvailable<float>("ParamEnergyLoss")) ?        toGeV(  muon->auxdata<float>("ParamEnergyLoss")          )  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_ParamELossSigmaM" , (mstrack && muon->isAvailable<float>("ParamEnergyLossSigmaMinus")) ? toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaMinus"))  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_ParamELossSigmaP" , (mstrack && muon->isAvailable<float>("ParamEnergyLossSigmaPlus")) ?  toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaPlus") )  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_nPres" ,       (mstrack && muon->isAvailable<unsigned char>("numberOfPrecisionLayers")) ?     muon->auxdata< unsigned char >("numberOfPrecisionLayers")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_nPresGood" ,   (mstrack && muon->isAvailable<unsigned char>("numberOfGoodPrecisionLayers")) ? muon->auxdata< unsigned char >("numberOfGoodPrecisionLayers")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_nPresHole" ,   (mstrack && muon->isAvailable<unsigned char>("numberOfPrecisionHoleLayers")) ? muon->auxdata< unsigned char >("numberOfPrecisionHoleLayers")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_qop"         , (mstrack) ?  mstrack->qOverP()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MStrack_qop_err"     , (mstrack) ?  mstrack->definingParametersCovMatrix()(4,4) / fabs(mstrack->qOverP())  : -999 ));
        
        //Extrapolated tracks
        const xAOD::TrackParticle* metrack = mu->trackParticle( xAOD::Muon::ExtrapolatedMuonSpectrometerTrackParticle );
        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_hasMEtrack" , (metrack) ? 1 : 0 ));

        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_eta" , (metrack) ?     metrack->eta()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_phi" , (metrack) ?     metrack->phi()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_pt"  , (metrack) ?     toGeV( metrack->pt() )   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_D0"  , (metrack) ?     metrack->d0()   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_Z0"  , (metrack) ?     metrack->z0()   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_chi2"        , (metrack && metrack->isAvailable<float>("chiSquared") ) ?  metrack->auxdata< float >("chiSquared")/metrack->auxdata< float >("numberDoF")  : -999 ));
        //STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_chi2"        , (metrack) ?  metrack->chiSquared()/metrack->auxdata< float >("numberDoF")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_qop"         , (metrack) ?  metrack->qOverP()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_MEtrack_qop_err"     , (metrack) ?  metrack->definingParametersCovMatrix()(4,4) / fabs(metrack->qOverP())  : -999 ));

        //Combined Tracks
        const xAOD::TrackParticle* cbtrack = mu->trackParticle( xAOD::Muon::CombinedTrackParticle );
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_hasCBtrack" , (cbtrack) ? 1 : 0 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_chi2" , (cbtrack && cbtrack->isAvailable<float>("chiSquared") ) ?   cbtrack->auxdata< float >("chiSquared")/cbtrack->auxdata< float >("numberDoF")  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_eta" , (cbtrack) ?     cbtrack->eta()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_phi" , (cbtrack) ?     cbtrack->phi()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_pt"  , (cbtrack) ?     toGeV( cbtrack->pt() )   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_D0"  , (cbtrack) ?     cbtrack->d0()   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_Z0"  , (cbtrack) ?     cbtrack->z0()   : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_qop"         , (cbtrack) ?  cbtrack->qOverP()  : -999 ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_CBtrack_qop_err"     , (cbtrack) ?  cbtrack->definingParametersCovMatrix()(4,4) / fabs(cbtrack->qOverP())  : -999 ));

        //Truth Particles
        const ElementLink< xAOD::TruthParticleContainer >& link = mu->auxdata< ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");

        if( link.isValid() ){
            const xAOD::TruthParticle* truth = *link;
            STRONG_CHECK(addToVectorBranch( vars, "muon_truthMatchedBarcode"  , truth->barcode()  )); 
            LinkedBarcodes.push_back(truth->barcode());
        }
        else
            STRONG_CHECK(addToVectorBranch( vars, "muon_truthMatchedBarcode" , -1                 ));



        float qOverPsigma  = -999.;
        float qOverPsignif = -999.;
        float rho = -999.;
        if ( (idtrack) && (metrack) && (cbtrack) ){
            float cbPt = cbtrack->pt();
            float idPt = idtrack->pt();
            float mePt = metrack->pt();
            float meP  = 1.0 / ( sin(metrack->theta()) / mePt);
            float idP  = 1.0 / ( sin(idtrack->theta()) / idPt);

            rho = fabs( idPt - mePt ) / cbPt;
            qOverPsigma  = sqrt( idtrack->definingParametersCovMatrix()(4,4) + metrack->definingParametersCovMatrix()(4,4) );
            qOverPsignif  = fabs( (metrack->charge() / meP) - (idtrack->charge() / idP) ) / qOverPsigma;
        }
        STRONG_CHECK(addToVectorBranch( vars, "muon_rho"   , rho ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_QoverPsignif", qOverPsignif ));

        STRONG_CHECK(addToVectorBranch( vars, "muon_author" ,     mu->auxdata< unsigned short >("author") ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_isCommonGood",mu->auxdata< char >("DFCommonGoodMuon") ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_isSignal",    mu->auxdata< char >("signal") ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_truthType",   mu->auxdata< int >("truthType") ));
        STRONG_CHECK(addToVectorBranch( vars, "muon_truthOrigin", mu->auxdata< int >("truthOrigin") ));

    nMuons++;
    }
    vars["muon_n"] = nMuons;
    
    ///////////////////////////////////////////////////////////////
    // Electrons + Muons in one branch
    // ////////////////////////////////////////////////////////////
    
    vars["lepton_n"] = nMuons + nElectrons;
    
    std::vector<xAOD::IParticle*> all_leptons_unsorted;
    for( auto  el : electrons_baseline) all_leptons_unsorted.push_back(el);
    for( auto  mu :muons_baseline) all_leptons_unsorted.push_back(mu);

    std::vector<xAOD::IParticle*> all_leptons = sortByPt(all_leptons_unsorted);
    int iLep = 0;
    int lep0_pdgId = -999;
    int lep1_pdgId = -999;
    float lep0_charge = -999;
    float lep1_charge = -999;
    TLorentzVector lep0;
    TLorentzVector lep1;

    for( const auto & lep : all_leptons ){
        STRONG_CHECK(addToVectorBranch( vars, "lepton_index"  , lep->index()));
        STRONG_CHECK(addToVectorBranch( vars, "lepton_pt"     , toGeV(lep->pt())));
        STRONG_CHECK(addToVectorBranch( vars, "lepton_eta"    , lep->p4().Eta()));
        STRONG_CHECK(addToVectorBranch( vars, "lepton_phi"    , lep->p4().Phi()));
        STRONG_CHECK(addToVectorBranch( vars, "lepton_passOR" , lep->auxdata<char>("passOR") ? 1 : 0));
        STRONG_CHECK(addToVectorBranch( vars, "lepton_charge" , lep->auxdata<float>("charge") ));
        
        
        if(iLep == 0){
            lep0_pdgId = lep->type(); 
            lep0_charge = lep->auxdata<float>("charge"); 
            lep0.SetPtEtaPhiE(toGeV(lep->pt()), lep->p4().Eta(), lep->p4().Phi(), toGeV(lep->e()));
        }
        if(iLep == 1){
            lep1_pdgId = lep->type(); 
            lep1_charge = lep->auxdata<float>("charge"); 
            lep1.SetPtEtaPhiE(toGeV(lep->pt()), lep->p4().Eta(), lep->p4().Phi(), toGeV(lep->e()));
        }
        
        if(lep->type() == 6){ //iParticles are dumb, see athena/Event/xAOD/xAODBase/xAODBase/ObjectType.h
            STRONG_CHECK(addToVectorBranch( vars, "lepton_pdgId" , 11*lep->auxdata<float>("charge") ));
            xAOD::Electron* l_el = dynamic_cast<xAOD::Electron*>( lep );
            STRONG_CHECK(addToVectorBranch( vars, "lepton_d0" , l_el->nTrackParticles() > 0 ? ((xAOD::TrackParticle*)l_el->trackParticle( 0 ))->d0() : -999 ));
        }
        else if( lep->type() == 8){
            STRONG_CHECK(addToVectorBranch( vars, "lepton_pdgId" , 13*lep->auxdata<float>("charge") ));
            xAOD::Muon* l_mu = dynamic_cast<xAOD::Muon*>( lep );
            const xAOD::TrackParticle* idtrack_m = l_mu->trackParticle( xAOD::Muon::InnerDetectorTrackParticle );
            STRONG_CHECK(addToVectorBranch( vars, "lepton_d0"       , idtrack_m ? float(idtrack_m->d0()) : -999 ));
        }
        else{
            STRONG_CHECK(addToVectorBranch( vars, "lepton_pdgId" , -999 ));
            STRONG_CHECK(addToVectorBranch( vars, "lepton_d0" , -999 ));
        }




        const ElementLink< xAOD::TruthParticleContainer >& link = lep->auxdata< ElementLink< xAOD::TruthParticleContainer > >("truthParticleLink");
        if( link.isValid() ){
            const xAOD::TruthParticle* truth = *link;
            STRONG_CHECK(addToVectorBranch( vars, "lepton_truthMatchedBarcode"  , truth->barcode()  )); 
        }
        else
            STRONG_CHECK(addToVectorBranch( vars, "lepton_truthMatchedBarcode" , -1                 ));
    iLep++;
    }
    
    //some general info
    //check that both leptons found
    if( lep0_charge == -999 || lep1_charge == -999){
        vars["isOS"] = -1;
        vars["channel"] = -1;
        vars["triggerRegion"] = -1;
        vars["triggerRegion_pass"] = -1;
        vars["invMass"] = -999.; 
        vars["deltaR"] = -999.;
        vars["deltaPhi"] = -999.;
        vars["deltaEta"] = -999.;
        vars["sumEta"] = -999.;

    }
    else{
        if(lep0_charge * lep1_charge > 0)
            vars["isOS"] = 0;
        else
            vars["isOS"] = 1;
        
        std::vector< std::string > passedTriggers = eventInfo->auxdecor<  std::vector< std::string >  >("passedTriggers");
       
        //channel and trigger regions
        //https://indico.cern.ch/event/786836/contributions/3270431/attachments/1803124/2941625/2019-02-22-FiltersTriggersCuts.pdf 
        if( fabs(lep0_pdgId) == 6 && fabs(lep1_pdgId) == 6 ){ //ee
            vars["channel"] = 0;
        }
        else if( fabs(lep0_pdgId) == 8 && fabs(lep1_pdgId) == 8 ){
            vars["channel"] = 1;
         }
        else if( fabs(lep0_pdgId) == 6 && fabs(lep1_pdgId) == 8){
            vars["channel"] = 2;
        }
        else if( fabs(lep0_pdgId) == 8 && fabs(lep1_pdgId) == 6 ){
            vars["channel"] = 3;
        }
        else{  
            vars["channel"] = -1;
        }
        
        //Angular distributions
        vars["invMass"] = (lep0 + lep1).M();
        vars["deltaR"] = lep0.DeltaR(lep1);
        vars["deltaPhi"] = lep0.DeltaPhi(lep1);
        vars["deltaEta"] = abs(lep0.Eta() - lep1.Eta());
        vars["sumEta"] = lep0.Eta() + lep1.Eta();

       //Trigger regions 
        if( (abs(lep0_pdgId) == 6 && lep0.Pt() > 160) || (abs(lep1_pdgId) == 6 && lep1.Pt() > 160) ){
            vars["triggerRegion"] = 0;
            if (std::find(passedTriggers.begin(), passedTriggers.end(), "HLT_g140_loose") != passedTriggers.end()){
                vars["triggerRegion_pass"] = 1;
            }
            else{
                vars["triggerRegion_pass"] = 0;
            }

        }
        else if( abs(lep0_pdgId) == 6 && abs(lep1_pdgId) == 6 ){ 
            vars["triggerRegion"] = 1;
            if(std::find(passedTriggers.begin(), passedTriggers.end(), "HLT_2g50_loose") != passedTriggers.end() || std::find(passedTriggers.begin(), passedTriggers.end(), "HLT_2g50_loose_L12EM20VH") != passedTriggers.end()){
                vars["triggerRegion_pass"] = 1;
            }
            else{
                vars["triggerRegion_pass"] = 0;
            }

        }
        else{
            vars["triggerRegion"] = 2;
            if (std::find(passedTriggers.begin(), passedTriggers.end(), "HLT_mu60_0eta105_msonly") != passedTriggers.end()){
                vars["triggerRegion_pass"] = 1;
            }
            else{
                vars["triggerRegion_pass"] = 0;
            }
        }

    }


    ///////////////////////////////////////////////////////////////
    // MS Tracks
    // ////////////////////////////////////////////////////////////

    const xAOD::TrackParticleContainer* ms_tracks = nullptr;
    STRONG_CHECK( event->retrieve(ms_tracks, "MuonSpectrometerTrackParticles") );
    const xAOD::MuonContainer *muons = nullptr;
    STRONG_CHECK( event->retrieve(muons, "Muons") );


    for ( auto muon : *muons ){

        const xAOD::TrackParticle* mst = muon->trackParticle( xAOD::Muon::MuonSpectrometerTrackParticle );
        if (mst) {


            STRONG_CHECK(addToVectorBranch( vars, "mstrack_MuonIndex" ,  ( MStrkToMuonMap.find(mst->index()) == MStrkToMuonMap.end() ) ? -99 : MStrkToMuonMap[mst->index()] ));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_eta" ,    mst->eta() ));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_phi" ,    mst->phi() ));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_pt"  ,    toGeV( mst->pt() )  ));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_D0"  ,    mst->d0()  ));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_Z0"  ,    mst->z0()  ));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_ELoss" ,            (muon->isAvailable<float>("EnergyLoss")                ? toGeV(  muon->auxdata<float>("EnergyLoss")   	          ) : -999. )));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_ELossSigma" ,       (muon->isAvailable<float>("EnergyLossSigma")           ? toGeV(  muon->auxdata<float>("EnergyLossSigma") 	       ) : -999. )));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_MeasELoss" ,        (muon->isAvailable<float>("MeanEnergyLoss")            ? toGeV(  muon->auxdata<float>("MeasEnergyLoss") 	          ) : -999. )));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_MeasELossSigma" ,   (muon->isAvailable<float>("MeanEnergyLossSigma")       ? toGeV(  muon->auxdata<float>("MeasEnergyLossSigma")       ) : -999. )));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_ParamELoss" ,       (muon->isAvailable<float>("ParamEnergyLoss")           ? toGeV(  muon->auxdata<float>("ParamEnergyLoss")           ) : -999. )));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_ParamELossSigmaM" , (muon->isAvailable<float>("ParamEnergyLossSigmaMinus") ? toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaMinus") ) : -999. )));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_ParamELossSigmaP" , (muon->isAvailable<float>("ParamEnergyLossSigmaPlus")  ? toGeV(  muon->auxdata<float>("ParamEnergyLossSigmaPlus")  ) : -999. )));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_nPres" ,      muon->auxdata< unsigned char >("numberOfPrecisionLayers") ));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_nPresGood" ,  muon->auxdata< unsigned char >("numberOfGoodPrecisionLayers") ));
            STRONG_CHECK(addToVectorBranch( vars, "mstrack_nPresHole" ,  muon->auxdata< unsigned char >("numberOfPrecisionHoleLayers") ));
        }
    }

    ///////////////////////////////////////////////////////////////
    // Inner Detector Tracks
    // ////////////////////////////////////////////////////////////

    if (m_saveIDtracks) {

        const xAOD::TrackParticleContainer* id_tracks = nullptr;
        STRONG_CHECK( event->retrieve(id_tracks, "InDetTrackParticles") );
        STRONG_CHECK(addToVectorBranch( vars, "n_idTracks" , id_tracks->size() ));

        for (auto *id_track : *id_tracks){

            // Save ID tracks if back to back in dR cosmic with muons
            int cos_tag = 0;
            for( xAOD::IParticle * muon : muons_baseline){
                xAOD::Muon* mu = dynamic_cast<xAOD::Muon*>(muon);
                if ( cosmic_Tag_highd0(id_track->p4(), mu->p4() ) ) cos_tag=1; 
            }


            	if ( toGeV( id_track->pt() ) < 10  && !cos_tag) continue; //pt cut

            STRONG_CHECK(addToVectorBranch( vars, "idTrack_index" , id_track->index() ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_cosTag", cos_tag ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_isLRT" , (id_track->isAvailable<unsigned long>("patternRecoInfo")) ? static_cast<int>( id_track->patternRecoInfo().test(xAOD::SiSpacePointsSeedMaker_LargeD0) ) : -999 ));


            // From https://svnweb.cern.ch/trac/atlasoff/browser/InnerDetector/InDetValidation/InDetPhysValMonitoring/trunk/src/InDetPerfPlot_resITk.cxx#L800
            double id_pt = id_track->pt();
            double id_diff_qp = -id_pt / std::fabs(id_track->qOverP());
            double id_diff_theta = id_pt / tan(id_track->theta());
            const std::vector<float> &id_cov = id_track->definingParametersCovMatrixVec();
            double id_pt_err2 = id_diff_qp * (id_diff_qp * id_cov[14] + id_diff_theta * id_cov[13]) + id_diff_theta * id_diff_theta * id_cov[9];
            double id_errpT = toGeV( TMath::Sqrt(id_pt_err2) );

            STRONG_CHECK(addToVectorBranch( vars, "idTrack_errPt"   , id_errpT ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_errd0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(0, 0)) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_errz0"   , TMath::Sqrt(id_track->definingParametersCovMatrix()(1, 1)) ));

            STRONG_CHECK(addToVectorBranch( vars, "idTrack_theta"   ,id_track->theta() ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_eta"     ,id_track->eta() ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_phi"     ,id_track->phi() ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_pt"      ,toGeV( id_track->pt() ) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_d0"      ,id_track->d0()  ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_z0"      ,id_track->z0()  ));
            //	STRONG_CHECK(addToVectorBranch( vars, "idTrack_z0WrtPV" ,id_track->z0() + id_track->vz() - pv->z() ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_charge"  ,id_track->charge() ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_chi2"    , (id_track->isAvailable<float>("chiSquared")) ? id_track->auxdata< float >("chiSquared")/(id_track->auxdata< float >("numberDoF")) : -999 ));
            //STRONG_CHECK(addToVectorBranch( vars, "idTrack_chi2"    ,id_track->chiSquared()/(id_track->auxdata< float >("numberDoF")) ));

            STRONG_CHECK(addToVectorBranch( vars, "idTrack_RFirstHit"     , (id_track->isAvailable<float>("radiusOfFirstHit") ) ? id_track->auxdata< float >("radiusOfFirstHit") : -999 ));

            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NPix_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfPixelHits"       ) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NSct_Hits"       ,(int) id_track->auxdata< unsigned char >("numberOfSCTHits"         ) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NTrt_Hits"       ,(id_track->isAvailable<unsigned char>("numberOfTRTHits")) ? (int) id_track->auxdata< unsigned char >("numberOfTRTHits" ) : -999 ));      
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NPix_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfPixelHoles"      ) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NSct_Holes"      ,(int) id_track->auxdata< unsigned char >("numberOfSCTHoles"        ) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NTrt_Outliers"   ,(int) id_track->auxdata< unsigned char >("numberOfTRTOutliers"     ) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NPix_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfPixelDeadSensors") ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NPix_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfPixelSharedHits" ) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NSct_DeadSens"   ,(int) id_track->auxdata< unsigned char >("numberOfSCTDeadSensors"  ) ));
            STRONG_CHECK(addToVectorBranch( vars, "idTrack_NSct_ShrHits"    ,(int) id_track->auxdata< unsigned char >("numberOfSCTSharedHits"   ) ));
        }

    }// end if save id tracks
    
    if (m_saveSegments){
        const xAOD::MuonSegmentContainer* ms_segments = nullptr;
        STRONG_CHECK( event->retrieve(ms_segments, "MuonSegments") );

        for (auto *ms_segment : *ms_segments){

            STRONG_CHECK(addToVectorBranch( vars, "msSegment_x"            , ms_segment->x() ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_y"            , ms_segment->y() ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_z"            , ms_segment->z() ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_px"           , ms_segment->px() ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_py"           , ms_segment->py() ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_pz"           , ms_segment->pz() ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_t0"           , ms_segment->auxdataConst< float >( "t0" ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_t0Err"        , ms_segment->auxdataConst< float >( "t0error"      ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_clusTimeErr"  , ms_segment->auxdataConst< float >( "clusterTimeError" ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_clusTime"     , ms_segment->auxdataConst< float >( "clusterTime"      ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_chmbIndex"    , ms_segment->auxdataConst< int >( "chamberIndex" ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_tech"         , ms_segment->auxdataConst< int >( "technology"   ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_sector"       , ms_segment->auxdataConst< int >( "sector"       ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_etaIndex"     , ms_segment->auxdataConst< int >( "etaIndex"     ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_nTrigEtaLays" , ms_segment->auxdataConst< int >( "nTrigEtaLayers" ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_nPhiLays"     , ms_segment->auxdataConst< int >( "nPhiLayers"     ) ));
            STRONG_CHECK(addToVectorBranch( vars, "msSegment_nPresHits"    , ms_segment->auxdataConst< int >( "nPrecisionHits" ) ));

        }

    }//end doCosmic



    ///////////////////////////////////////////////////////////////
    // Truth info
    // ///////////////////////////////////////////////////////////
    if ( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) ){

        // get the truth events to get the true signal vertex position
        const xAOD::TruthEventContainer* truthEvents = 0;
        STRONG_CHECK( event->retrieve( truthEvents, "TruthEvents" ));
        // auto trueSignalVertex = *((*truthEvents->begin())->truthVertexLink(0)); // not in DAOD_PHYS style derivations

        // ALTERNATIVE OPTIONS TRUTH VERTICES R22
        const xAOD::TruthVertexContainer* truthVertices = 0;
        const xAOD::TruthVertex* trueSignalVertex = 0;
        //STRONG_CHECK( event->retrieve( truthVertices, "TruthPrimaryVertices" ));
        //if (truthVertices->size()>0) { trueSignalVertex = truthVertices->at(0); }
        //STRONG_CHECK( event->retrieve( truthVertices, "HardScatterVertices" ));
        //if (truthVertices->size()>0) { trueSignalVertex = truthVertices->at(0); }
        STRONG_CHECK( event->retrieve( truthVertices, "TruthBSMWithDecayVertices" ));
        if (truthVertices->size()>0) { trueSignalVertex = truthVertices->at(0); }


        // now get the truth particles
        const xAOD::TruthParticleContainer* truthParticles = 0;
        //STRONG_CHECK( event->retrieve( truthParticles, "TruthParticles" ) ); // not in DAOD_PHYS style derivations
        STRONG_CHECK( event->retrieve( truthParticles, "TruthBSMWithDecayParticles" ) );
       
        //so that the branch comes out pt ordered 
        std::vector<int> toSave;
        toSave.clear();
         
        for (const auto & p : *truthParticles){

            if(fabs(p->pdgId())>1e6 && p->hasDecayVtx() && p->barcode() < 200000 && p->status() == 1) {

                float lifetimeLab = (p->decayVtx()->v4().Vect()-trueSignalVertex->v4().Vect()).Mag()/(p->p4().Vect().Mag()/(p->p4().Gamma()*p->p4().M())*TMath::C());
                TVector3 sleptonPos(p->decayVtx()->x() - trueSignalVertex->x(), p->decayVtx()->y() - trueSignalVertex->y(), p->decayVtx()->z() - trueSignalVertex->z()) ;


                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_pdgId"               , (int) p->pdgId()                ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_pt"                  , toGeV(p->pt() )                 ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_eta"                 , p->eta()                        ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_phi"                 , p->phi()                        ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_M"                   , toGeV(p->m())                   ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_betaGamma"           , p->p4().Beta()*p->p4().Gamma()  ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_barcode"             , p->barcode()                    ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_properDecayTime"     , lifetimeLab*p->p4().Gamma()     ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxX"                , p->decayVtx()->x()              ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxY"                , p->decayVtx()->y()              ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxZ"                , p->decayVtx()->z()              ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_VtxNParticles"       , p->decayVtx()->nOutgoingParticles()  ));
                STRONG_CHECK(addToVectorBranch( vars, "truthSparticle_truthOrigin"         , p->isAvailable<int>("truthOrigin") ? p->auxdata<int>("truthOrigin") : -999  ));


                for(size_t ic = 0; ic < p->nChildren(); ic++){

                    if( !p->child(ic)) continue;
                    auto slep_child = p->child(ic);
                    int abs_pdgid = fabs(slep_child->pdgId());

                    if ( abs_pdgid == 11 || abs_pdgid == 13 || abs_pdgid == 15 ) { // Save all truth leptons coming from sleptons
                            if ( std::find(toSave.begin(), toSave.end(), slep_child->barcode()) == toSave.end() )
                                toSave.push_back(slep_child->barcode()); 
                            
                            if ( abs_pdgid == 15 ) { // For taus, also save the truth leptons resulting from their decay
                                for(size_t igc = 0; igc < slep_child->nChildren(); igc++){
                                    
                                    if ( !slep_child->child(igc) ) continue;
                                    auto slep_gchild = slep_child->child(igc);
                                    int abs_pdgid_gc = fabs(slep_gchild->pdgId());
                                    
                                    if ( abs_pdgid_gc == 11 || abs_pdgid_gc == 13 )
                                        if ( std::find(toSave.begin(), toSave.end(), slep_gchild->barcode()) == toSave.end() )
                                            toSave.push_back(slep_gchild->barcode());

                                }
                            }

                    }
                }
            } //if truth sparticle      
            else if( std::find(LinkedBarcodes.begin(), LinkedBarcodes.end(), p->barcode()) != LinkedBarcodes.end() && std::find(toSave.begin(), toSave.end(), p->barcode()) == toSave.end() ) { //if the barcode is one that is linked to a reco'd particle, but not already saved, save it
                
                toSave.push_back( p->barcode() );
            }
        }

        //now loop to save the objects matched to reco leptons or from the truth sparticle in pt order
        for (const auto & p : *truthParticles){

            //if we have decided to save this particle
            if( std::find(toSave.begin(), toSave.end(), p->barcode()) != toSave.end() ) {

                float d0 = -999.;
                TVector3 sleptonPos(p->prodVtx()->x() - trueSignalVertex->x(), p->prodVtx()->y() - trueSignalVertex->y(), p->prodVtx()->z() - trueSignalVertex->z()) ;
                TVector3 lepton;
                lepton.SetMagThetaPhi(1.0,p->p4().Theta(),p->phi());
                d0 = sleptonPos.Perp() * sin(fabs(lepton.DeltaPhi(sleptonPos)));

                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_pdgId"                  , p->pdgId()       ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_index"                  , p->index()       ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_pt"                     , toGeV(p->pt())   ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_eta"                    , p->eta()         ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_phi"                    , p->phi()         ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_M"                      , toGeV(p->m())    ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_d0"                     , d0               ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_barcode"                , p->barcode()     ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_nParents"               , p->nParents()    ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_parentBarcode"          , (p->nParents() > 0) ? p->parent(0)->barcode() : -999    ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_parentPdgId"            , (p->nParents() > 0) ? p->parent(0)->pdgId()   : -999    ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_VtxX"                   , p->prodVtx()->x()              ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_VtxY"                   , p->prodVtx()->y()              ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_VtxZ"                   , p->prodVtx()->z()              ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_generatorParent_pdgId"  , getGeneratorParent(p)->pdgId()     ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_generatorParent_barcode", getGeneratorParent(p)->barcode()     ));
                STRONG_CHECK(addToVectorBranch( vars, "truthLepton_truthOrigin"            , p->isAvailable<int>("truthOrigin") ? p->auxdata<int>("truthOrigin") : -999 ));
            }
        }
    }



return EL::StatusCode::SUCCESS;
}


EL::StatusCode RegionVarCalculator_highd0::doSR0LCalculations(std::map<std::string, anytype>& /*vars*/ )
{
    return EL::StatusCode::SUCCESS;
}



EL::StatusCode RegionVarCalculator_highd0::doSR2LCalculations(std::map<std::string, anytype>& /*vars*/ )
{

    return EL::StatusCode::SUCCESS;

}


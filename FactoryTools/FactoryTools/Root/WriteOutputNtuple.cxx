#include <EventLoop/Job.h>
#include <EventLoop/StatusCode.h>
#include <EventLoop/Worker.h>

#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"

#include "SUSYTools/SUSYObjDef_xAOD.h"

#include <FactoryTools/WriteOutputNtuple.h>

#include <FactoryTools/printDebug.h>
#include <FactoryTools/strongErrorCheck.h>
#include "xAODEventInfo/EventInfo.h"

#include <FactoryTools/HelperFunctions.h>

#include <boost/algorithm/string.hpp>

#include <iostream>

// this is needed to distribute the algorithm to the workers
ClassImp(WriteOutputNtuple)

typedef FactoryTools::HelperFunctions HF;


WriteOutputNtuple :: WriteOutputNtuple ()
: m_tm(nullptr) {}

EL::StatusCode WriteOutputNtuple :: setupJob (EL::Job& /*job*/) {return EL::StatusCode::SUCCESS;}
EL::StatusCode WriteOutputNtuple :: histInitialize () {return EL::StatusCode::SUCCESS;}
EL::StatusCode WriteOutputNtuple :: fileExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode WriteOutputNtuple :: changeInput (bool /*firstFile*/) {return EL::StatusCode::SUCCESS;}


EL::StatusCode WriteOutputNtuple :: initialize ()
{
  ATH_MSG_INFO("writing to output file : " << outputName );
  ATH_MSG_INFO("region name            : " << regionName );
  ATH_MSG_INFO("systematic name        : " << systVar.name() );
  ATH_MSG_INFO("writing to tree        : " << regionName + systVar.name() );

  m_tm.reset( new TreeManager );
  m_tm->initialize( outputName+"_"+regionName+"_"+systVar.name(), wk()->getOutputFile(outputName));//todo make the treename smarter

  return EL::StatusCode::SUCCESS;
}



EL::StatusCode WriteOutputNtuple :: execute ()
{

  xAOD::TEvent* event = wk()->xaodEvent();

  auto eventInfo = HF::grabFromEvent<xAOD::EventInfo>("EventInfo",event);

  // If it hasn't been selected in any of the regions from any of the select algs, don't bother writing anything out
  // Note: You can put in a "post-pre-selection" which can cut on e.g. RJR vars and set the region to a blank string
  // So this needn't be logically the same as the decision in CalculateRJigsawVariables::execute()

  std::string const & eventRegionName =  eventInfo->auxdecor< std::string >("regionName");
  ATH_MSG_DEBUG("Event falls in region: " << eventRegionName  );

  if (HF::checkForSkip(eventInfo,false)) return EL::StatusCode::SUCCESS;

  // Furthermore! If the event doesn't pass this region def, don't write it out to this tree.
  if( eventRegionName != regionName ){
    ATH_MSG_INFO("eventRegionName does NOT match regionName!\n    EventRegionName = "<<eventRegionName<<"\n    regionName = "<<regionName);
	  return EL::StatusCode::SUCCESS;
  }

  ATH_MSG_DEBUG("Our event passed one of the selections " << eventRegionName);
  ATH_MSG_DEBUG("Storing map in output " << regionName  );

  for( auto& collection : TreeManager::collections() ) {

    ATH_MSG_DEBUG( "Variable collection [ " << collection.first << " ]: size = " << collection.second.size() );

    for( auto& pair : collection.second ) {

      if( msg().msgLevel( MSG::DEBUG ) ) {
        std::cout << "  " << pair.first << ": ";
        pair.second.print();
      }

      m_tm->book( pair.first.c_str(), pair.second );

    }
  }

  m_tm->fill();

  // clear vars
  for( auto& collection : TreeManager::collections() ) {
    TreeManager::clearVars( collection.first );
  }

  printDebug();

  return EL::StatusCode::SUCCESS;
}


EL::StatusCode WriteOutputNtuple :: postExecute () {return EL::StatusCode::SUCCESS;}
EL::StatusCode WriteOutputNtuple :: finalize () {
  // Chiming a bell at the end of the job for convenience
  std::cout << "\a" << std::endl;
  return EL::StatusCode::SUCCESS;
}
EL::StatusCode WriteOutputNtuple :: histFinalize () {return EL::StatusCode::SUCCESS;}

#include "FactoryTools/VsiAnalysis/VsiEventInfoProcessor.h"
#include <xAODEventInfo/EventInfo.h>

VsiEventInfoProcessor::VsiEventInfoProcessor( std::string name )
  : ProcessorBase( name )
{}



VsiEventInfoProcessor::~VsiEventInfoProcessor()
{}


void VsiEventInfoProcessor::registerVariables() {

  m_vars["runNumber"]              = uint32_t { 0 };
  m_vars["eventNumber"]            = 0ULL;
  m_vars["lumiBlock"]              = uint32_t { 0 };
  m_vars["averageMu"]              = 0.0F;
  m_vars["actualMu"]               = 0.0F;
  m_vars["regionName"]             = std::string{};

}


EL::StatusCode VsiEventInfoProcessor::processDetail( xAOD::TEvent* event, xAOD::TStore* /*store*/ ) {

  const xAOD::EventInfo              *eventInfo        ( nullptr );

  // retrieving
  STRONG_CHECK( event->retrieve( eventInfo, "EventInfo") );

  setMC( eventInfo->eventType( xAOD::EventInfo::IS_SIMULATION ) );

  REFVAR( runNumber,       uint32_t);
  REFVAR( eventNumber,     unsigned long long);
  REFVAR( lumiBlock,       uint32_t);
  REFVAR( averageMu,       float);
  REFVAR( actualMu,        float);
  REFVAR( regionName,      std::string);

  r_runNumber   = eventInfo->runNumber();
  r_eventNumber = eventInfo->eventNumber();
  r_lumiBlock   = eventInfo->lumiBlock();
  r_averageMu   = eventInfo->averageInteractionsPerCrossing();
  r_actualMu    = eventInfo->actualInteractionsPerCrossing();

  if( eventInfo->isAvailable<std::string>("regionName") ) {
    r_regionName  = eventInfo->auxdata<std::string>("regionName");
  } else {
    r_regionName  = std::string{};
  }

  return EL::StatusCode::SUCCESS;
}

#include <FactoryTools/VsiAnalysis/VsiTruthHelper.h>

#include <xAODTruth/TruthParticle.h>

namespace VsiTruthHelper {


  bool selectRhadron(const xAOD::TruthVertex* truthVertex ) {

    if( truthVertex->perp() < 0.1 )                                   return false;
    if( truthVertex->nIncomingParticles() != 1 )                      return false;
    auto& links = truthVertex->incomingParticleLinks();
    if( links.size() != 1 ) return false;
    if( !links.at(0).isValid() ) return false;
    if( abs(truthVertex->incomingParticle(0)->pdgId()) < 1000000 )    return false;
    if( abs(truthVertex->incomingParticle(0)->pdgId()) > 1000000000 ) return false; // Nuclear codes, e.g. deuteron

    // check if the R-hadron daughter exists
    bool hasNeutralino = false;
    for( unsigned ip = 0; ip < truthVertex->nOutgoingParticles(); ip++ ) {
      auto* p = truthVertex->outgoingParticle(ip);
      if( abs( p->pdgId() ) == 1000022 ) {
        hasNeutralino = true;
        break;
      }
    }
    if( !hasNeutralino ) {
      return false;
    }
    return true;
  }


  bool selectBmeson(const xAOD::TruthVertex* truthVertex ) {
    if( truthVertex->perp() < 1.0 )                                   return false;
    if( truthVertex->nIncomingParticles() != 1 )                      return false;
    auto& links = truthVertex->incomingParticleLinks();
    if( links.size() != 1 ) return false;
    if( !links.at(0).isValid() ) return false;
    auto* particle = *( links.at(0) );
    if( !particle )                                                   return false;
    auto pdgId = abs(particle->pdgId());
    if( ! (pdgId>500 && pdgId<600) )                                  return false;

    return true;
  }


  bool selectDarkPhoton(const xAOD::TruthVertex* truthVertex ) {
    if( truthVertex->perp() < 0.1 )                                   return false;
    if( truthVertex->nIncomingParticles() != 1 )                      return false;
    auto& links = truthVertex->incomingParticleLinks();
    if( links.size() != 1 ) return false;
    if( !links.at(0).isValid() ) return false;
    if( abs(truthVertex->incomingParticle(0)->pdgId()) != 4900111 )   return false;

    return true;
  }

  bool selectKshort(const xAOD::TruthVertex* truthVertex ) {
    if( truthVertex->nIncomingParticles() != 1 )                      return false;
    auto& links = truthVertex->incomingParticleLinks();
    if( links.size() != 1 ) return false;
    if( !links.at(0).isValid() ) return false;
    if( abs(truthVertex->incomingParticle(0)->pdgId()) != 310 )       return false;
    return true;
  }


  bool selectHadInt(const xAOD::TruthVertex* truthVertex ) {
    if( truthVertex->perp() < 20.0 )                                  return false;
    if( truthVertex->nIncomingParticles() != 1 )                      return false;
    auto& links = truthVertex->incomingParticleLinks();
    if( links.size() != 1 ) return false;
    if( !links.at(0).isValid() ) return false;

    auto* parent = truthVertex->incomingParticle(0);
    if( !parent ) return false;
    if( parent->isLepton() )                                          return false;

    TLorentzVector p4sum_in;
    TLorentzVector p4sum_out;
    for( unsigned ip = 0; ip < truthVertex->nIncomingParticles(); ip++ ) {
      auto* particle = truthVertex->incomingParticle(ip);
      TLorentzVector p4; p4.SetPtEtaPhiM( particle->pt(), particle->eta(), particle->phi(), particle->m() );
      p4sum_in += p4;
    }
    for( unsigned ip = 0; ip < truthVertex->nOutgoingParticles(); ip++ ) {
      auto* particle = truthVertex->outgoingParticle(ip);
      if( !particle ) continue;
      TLorentzVector p4; p4.SetPtEtaPhiM( particle->pt(), particle->eta(), particle->phi(), particle->m() );
      p4sum_out += p4;
    }
    if( p4sum_out.E() - p4sum_in.E() < 100. )                         return false;
    return true;
  }



  const xAOD::TruthParticle* getTruthParticle       ( const xAOD::TrackParticle* trk ) {

    typedef ElementLink<xAOD::TruthParticleContainer> truthLink;
    const xAOD::TruthParticle *truth { nullptr };

    if( trk->isAvailable< truthLink >("truthParticleLink") ) {
      try {
        const truthLink& link = trk->auxdataConst< truthLink >("truthParticleLink");
        truth = *link;
      } catch(...) {}
    }
    return truth;

  }


  const xAOD::TruthParticle* getParentTruthParticle ( const xAOD::TrackParticle* trk ) {
    const auto* truthParticle = getTruthParticle( trk );

    if( !truthParticle ) return nullptr;

    return truthParticle->parent(0);
  }



  const xAOD::TruthVertex*   getProdVtx             ( const xAOD::TrackParticle* trk ) {
    const auto* truthParticle = getTruthParticle( trk );

    if( !truthParticle ) return nullptr;

    return truthParticle->prodVtx();
  }



  bool isReconstructed( const xAOD::TruthParticle* particle, const xAOD::TrackParticleContainer* tracks ) {

    bool flag { false };

    if( fabs( particle->charge() ) > 0. ) {
      for( auto* trk : *tracks ) {
        const auto* truth = VsiTruthHelper::getTruthParticle( trk );
        if( particle == truth ) {
          flag = true;
          break;
        }
      }
    }
    return flag;
  }
  
  const xAOD::TrackParticle* getRecoTrack( const xAOD::TruthParticle* particle, const xAOD::TrackParticleContainer* tracks ) { 
        
    const xAOD::TrackParticle* matchedTrack = 0;
    if( fabs( particle->charge() ) > 0. ) { 
      for( auto* trk : *tracks ) { 
        const auto* truth = VsiTruthHelper::getTruthParticle( trk );
        if( particle == truth ) { 
           matchedTrack = trk;
          break;
        }   
      }   
    }   
    return matchedTrack;
  }

}

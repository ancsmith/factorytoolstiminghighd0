#!/usr/bin/env python

########### Initialization ######################################
##
##

import ROOT
import logging
import shutil
import os, sys
import itertools
import array
import glob
import re

import discoverInput

logging.basicConfig(level=logging.INFO)
from optparse import OptionParser

import atexit
@atexit.register
def quite_exit():
	ROOT.gSystem.Exit(0)

ROOT.gROOT.SetBatch()
import multiprocessing as mp

from collections import OrderedDict
import pickle

def main():
	"""
	Main function for user interface and steering across multiple SHs from an unstructured list of DIDs
	"""

	logging.info("loading packages...")
	ROOT.gROOT.Macro("$ROOTCOREDIR/scripts/load_packages.C")

	parser = OptionParser()
	parser.add_option("--inDir", help   = "dir with output", default="")
	parser.add_option("--inRucioDS", help   = "file with rucio DS names", default="")
	parser.add_option("--rse", help   = "grid location to search", default="CERN-PROD_LOCALGROUPDISK")
	parser.add_option("--nproc", help     = "number of parallel processes", default="4"  )
	parser.add_option("--selection", help     = "selection string for skimming", default="1."  )
	parser.add_option("--outputDir", help ="where to write the output merged ntuples" , default = "output")
	parser.add_option("--treePrefix", help ="tree name prefix" , default = "")
	parser.add_option("--keepTmpFiles", help ="keep files per dsid before merging. don't use for very large merging jobs." , action='store_true', default = False)
	parser.add_option("--dryRun",  help = 'do a dry run, without actually launching the merge job', action='store_true', default=False)
	parser.add_option("--testRun",  help = 'run on one file', action='store_true', default=False)
	parser.add_option("--blindDataSelection",  help = 'blind the data. specify SR to blind.', default="")
	parser.add_option("--tagConfig",  help = 'points to a python file that contains a function for defining tags', default=os.environ['FT']+"/util/defaultTags.py")
	parser.add_option("--metaDataFolder",  help = "points to a folder of manual metadata files for custom cross sections", default="")
	parser.add_option("--metaDataFile",  help = "points to a metadata file for custom cross sections. default looks at central PMG DB", default="")
	parser.add_option("--branchExclude",  help = "(comma separated) branches to exclude from output. Same as slimming. ", default="")

	(options, args) = parser.parse_args()


	print(options)

	# Going to use multiple cores here. But should cap it at the machine's CPU count
	ncores = min(int(options.nproc),mp.cpu_count())


	# Pulling in some per-analysis level configs. Defines outputSameNames and tagConfigFunction
	# A bit sloppy, I know...
	exec(open(options.tagConfig).read())

	# Create the output dirs
	if not os.path.exists(options.outputDir):
	    os.makedirs(options.outputDir)
	if not os.path.exists(options.outputDir+"/signal"):
	    os.makedirs(options.outputDir+"/signal")


	logging.info("creating new sample handler")
	# This is the master SH that contains all of the samples. Just throw everything in here
	sh_all = ROOT.SH.SampleHandler()

	# Can't do both rucio and local inputs simultaneously
	if options.inRucioDS and options.inDir:
		print("ERROR: You've pointed to both local files and rucio datasets - you probably shouldn't do that... Exiting")
		sys.exit()

	# But you need something...
	elif options.inRucioDS=="" and options.inDir=="":
		print("ERROR: You've got to give me something to work with... use --inRucioDS or --inDir")
		sys.exit()

	# If we're reading these DIDs off of a Rucio Storage Element (rse)
	if options.inRucioDS:
		print("Reading Rucio datasets from "+options.inRucioDS)
		print("Going to look at grid location "+options.rse)
		tmpfile = open(options.inRucioDS, "r").read().splitlines();

		# One day we can clean this up and just import this functionality because this is a bit clumsy.
		os.system("python $FT/python/buildRSEFileList.py --didfile %s --rse %s"%(options.inRucioDS,options.rse) )
		fileDictionary = pickle.load( open( "files.pkl", "rb" ) )

		for line in tmpfile:
			fileDictionary[line] = [re.sub(r"([a-zA-Z0-9])\/([a-zA-Z0-9_])",r"\1//\2", x, count=1) for x in fileDictionary[line] ]
			discoverInput.discoverFileList( sh_all, fileDictionary[line], line )
			print(sh_all.Print())

	# If we're reading these containers locally. i.e. you've downloaded the datasets manually
	if options.inDir:
		print("Reading local datasets from "+options.inDir)
		search_directories = [options.inDir]
		discoverInput.discover(sh_all, search_directories, "*"  )#todo add options

	print('mergeOutput: Length of sh_all : ', len(sh_all))

	logging.info("adding my tags defined in discoverInput.py")

	# Adding tags. These are defined in discoverInput and are crucial for splitting the
	#     master SH into smaller physics-sorted ones.
	discoverInput.addTags(sh_all,tagConfigFunction)

	# This is going to add the cross section information to all the samples in the master
	#     SH, as long as ST knows about it. Can hand this manual files in the future as well
	ROOT.SH.readSusyMetaDir(sh_all,"$FT/data/SUSYTools/mc15_13TeV/")
	if options.metaDataFolder:
		ROOT.SH.readSusyMetaDir(sh_all,options.metaDataFolder)

	# This is for grabbing cross section meta data from a PMG style XS DB. Default to mc16 on cvmfs
	try:
		getMetaDataPMG(sh_all,"/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt")
		if options.metaDataFile:
			getMetaDataPMG(sh_all,options.metaDataFile)
		if options.metaDataFolder:
			for filepath in glob.iglob(options.metaDataFolder+'/*.txt'):
				getMetaDataPMG(sh_all,filepath)
	except:
		print("mergeOutput: Failed to get PMG DB. Maybe you don't have CVMFS access?")

	print("mergeOutput: Run over sample handlers :", outputSampleNames)

	sh_list = OrderedDict()
	print("mergeOutput: Running over %d samples"%len(sh_all))

	# This is the actual sorting of samples into individual SHs
	#    For example, all the dijet samples are being sorted into 
	#    the QCD SH here.
	for outputSampleName in outputSampleNames:
		sh_list[outputSampleName] = sh_all.find(outputSampleName)

	if options.keepTmpFiles:
		print("mergeOutput: Keeping all temporary files.")

	if not options.dryRun :
		# Multiple core uses mp.Pool. For debugging sake, single core doesn't.
		if int(ncores)>1:
			pool = mp.Pool(processes=ncores)
			pool.map(processTheSHWrapper,
				itertools.izip( sh_list.values(),
					itertools.repeat( options.outputDir ),
					sh_list.keys(),
					itertools.repeat( options.selection ),
					itertools.repeat( options.branchExclude ),
					itertools.repeat( options.keepTmpFiles ),
					itertools.repeat( options.treePrefix ),
					itertools.repeat( options.blindDataSelection ),
					 )
				)
			pool.close()
			pool.join()
		else:
			for outputSampleName,my_sh in sh_list.iteritems():
				processTheSHWrapper([my_sh, options.outputDir, outputSampleName, options.selection, options.branchExclude, options.keepTmpFiles, options.treePrefix])

	print("")
	print("mergeOutput: All done! Wasn't that easy? <3333 [Send donations to Lawrence Lee, Jr.]")

	return


def processTheSHWrapper(stuff):
	return processTheSH(*stuff)


def processTheSH( sh,
		  outputDirectory = "output",
		  sampleName = "OTHER.root",
		  selection  = "1.",
		  branchExclude = "",
  		  keepTmpFiles = False,
		  treePrefix = "",
		  blindDataSelection = "",
		  ) :
	"""
	This is the workhorse function that actually grabs all the samples for a given SH object, runs
	the creation of the normweight branch, and then merges the files.
	"""

	workerID = " (%d)"%(mp.current_process()._identity[0]) if mp.current_process()._identity else ""
	if workerID:
		print("Using multiple cores. This is process %s"%workerID)

	print("processTheSH%s: sampleName %s" % (workerID,sampleName))
	print("processTheSH%s: \t with %d samples"%(workerID, len(sh)))

	if blindDataSelection and "ata" in sampleName:
		selection = "(%s)&&((%s)==0)"%(selection,blindDataSelection)
		print("************** Blinding the SR in Data! *****************")

	treesToProcess = []

	tmpOutputDirectory = os.path.join(outputDirectory, "tmpOutput")

	# Will be a dictionary of trees that marks them as having events or not.
	hasEntries = {}

	# Looping over each DID in the aggregate SH
	for sample in sh :
		sample_name = sample.getMetaString("sample_name")
		dsid = sample.getMetaString("dsid")

		# Let's only grab the tree list for the first sample in the SH
		if len(treesToProcess) == 0:
			treesToProcess = getListOfTreeNames(sample, treePrefix)
                        
		isData = True if "Data" in sampleName or "data" in sampleName else False

		# Attaches the nevent and sumweight values to the sample object
		if not isData:
			attachCounters(sample)

		# Create temporary dirs
		if not os.path.exists(tmpOutputDirectory):
		    os.makedirs(tmpOutputDirectory)

		outputSampleFileName = "%s/%s.root"%(tmpOutputDirectory, dsid)

		outputSampleFile = ROOT.TFile(outputSampleFileName,"RECREATE")

		print("processTheSH%s: [ ] Starting to process sample %s"%(workerID, dsid))

		# Setting up flags for if trees have events (and therefore branches in FT)
		#     Merging would be unpredictable if presented with a tree with no branches
		for itree in treesToProcess:
			if itree not in hasEntries:
				hasEntries[itree] = {}
			hasEntries[itree][outputSampleFileName] = False

		# Get a normalization factor to add to the tree
		normFactor = getNormFactor(sample, isData)
		for itree in treesToProcess:
			sh.setMetaString("nc_tree", itree)
			print("processTheSH%s: \t Processing tree %s . . ." % ( workerID, itree ))

			outputSampleFile.cd()

			# Create a TChain object from the sample
			mytree = sample.makeTChain()

			# If it's data, don't bother adding the norm weight branch
			if isData:
				if mytree.GetEntries():
					if selection!="1.":
						# If you're applying event-level skimming, use CopyTree()
						outputTree = mytree.CopyTree(selection)
					else:
						# Else use the much faster CloneTree(-1,"fast")
						outputTree = mytree.CloneTree(-1,"fast")

					if branchExclude!="":
						setBranchStatus(outputTree,branchExclude,0)
						outputTree = outputTree.CloneTree()

					outputTree.Write()
					hasEntries[itree][outputSampleFileName] = True
			else:
				mytree = mytree.Clone(itree)

				if mytree.GetEntries():
					try:
						# Calling C++ event loop (defined below) to add norm weight
						outputTree = ROOT.addBranch( mytree, normFactor , selection)
					except:
						print('processTheSH%s: \t failed to add branch'%workerID)
						continue

					if branchExclude!="":
						setBranchStatus(outputTree,branchExclude,0)
						outputTree = outputTree.CloneTree()

					outputTree.Write()
					print("processTheSH%s: \t Saved tree %s with %s events!" % ( workerID, outputTree.GetName(), outputTree.GetEntries() ))
					hasEntries[itree][outputSampleFileName] = True
				else:
					mytree.Write()

		outputSampleFile.Write()
		outputSampleFile.Close()

		print("processTheSH%s: [x] Finished with sample %s"%(workerID, dsid))

	print("processTheSH%s: Writing to final output file for %s"%(workerID, sampleName))

	if "signal" in sampleName:
		# If it's signal, we don't want to hadd them together at all.
		#     Just copy them to the signal output directory.
		for itree in treesToProcess:
			for iFileName in hasEntries[itree]:
				if os.path.exists(iFileName):
				    shutil.copy(iFileName, outputDirectory+"/signal/")

	else:
		# If it's a background, let's just smush it all together into a single file!
		outputSHFile = ROOT.TFile("%s/%s.root"%(outputDirectory, sampleName) , "RECREATE")

		# This is a per-tree hadding that's a bit safer than explicitly calling hadd
		for itree in treesToProcess:
			outputChain = ROOT.TChain(itree)
			nFiles = 0
			for iFileName in hasEntries[itree]:
				if hasEntries[itree][iFileName]:
					nFiles = outputChain.Add(iFileName)
			outputSHFile.cd()
			if outputChain.GetNtrees():
				cloneTree = outputChain.CloneTree(-1,"fast")
				cloneTree.Write()
			else:
				tmpTree = ROOT.TTree()
				tmpTree.Write(itree)

	if not keepTmpFiles:
		for itree in treesToProcess:
			for iFileName in hasEntries[itree]:
				if os.path.exists(iFileName):
				    os.remove(iFileName)

	return


def getMetaDataPMG(sampleHandler, database):
	with open(database) as f:
	    lines = f.read().splitlines()
	for sample in sampleHandler:
		dsid = sample.getMetaString("dsid")
		selectedLines = [x for x in lines if dsid in x]
		# print(selectedLines[0].split())
		item = selectedLines[0].split()
		sample.setMetaDouble("nc_xs", float(item[2]) )
		sample.setMetaDouble("filter_efficiency", float(item[3]) )
		sample.setMetaDouble("kfactor", float(item[4]) )
		sample.setMetaDouble("XSecUncUP", float(item[5]) )
		sample.setMetaDouble("XSecUncDOWN", float(item[6]) )
	return



def getNormFactor(sample, isData = False):
	"""
	To scale the histograms in the files after the event loop is done...
	"""

	if isData : return 1.

	# Cross-section weights
	tempxs = sample.getMetaDouble("nc_xs") * sample.getMetaDouble("kfactor") * sample.getMetaDouble("filter_efficiency")

	print("getNormFactor: \t\t Norm weight for %s is %.2e/(%d (nevents for QCD) or %.4e (sum of weights) )"%(sample.getMetaString("short_name"), tempxs, sample.getMetaDouble("nc_nevt"), sample.getMetaDouble("nc_sumw")))
	m_eventscaling = tempxs
	if sample.getMetaDouble("nc_nevt"):
		# QCD is weighted by nevents instead of by sum of weights
                print("short_name:", sample.getMetaString("short_name"))
                if "jetjet" in sample.getMetaString("short_name") and not "WithSW"  in sample.getMetaString("short_name"):
                        m_eventscaling /= sample.getMetaDouble("nc_nevt")
                else:
                        m_eventscaling /= sample.getMetaDouble("nc_sumw")
	else:
		print("getNormFactor: nevt not SET! normweight = 0 !!!")
		m_eventscaling = 0.
	return m_eventscaling



addBranchCode = """
TTree * addBranch(TTree* tree, float normalization, TString selection="1"){

		TTree * newtree = tree->CopyTree(selection);
		float normweight = normalization;
		TBranch * bnormweight = newtree->Branch("normweight",&normweight,"normweight/F");
		int nevents = newtree->GetEntries();

		for (Long64_t i=0;i<nevents;i++) {
			newtree->GetEntry(i);
			bnormweight->Fill();
			if(i%10000==0) cout<< i << " of " << nevents << endl;
		}

		return newtree;
}
"""

# Adding the fast C++ function above to ROOT for use in the python. All of the actual event looping is done in C++/Clang.
ROOT.gInterpreter.Declare(addBranchCode)


def attachCounters(sample):
	"""
	Go get the MetaData histograms written by BasicEventSelection from the
	    original files and grab the number of events and sum of weights
	"""

	nevt = 0
	sumw = 0

	for fname in  sample.makeFileList() :
		f = ROOT.TFile.Open( fname )
		nevt += f.Get("MetaData_EventCount").GetBinContent(1) #"nEvents initial"
		sumw += f.Get("MetaData_EventCount").GetBinContent(3) #"sumOfWeights initial"
		f.Close()

	sample.setMetaDouble("nc_nevt",nevt)
	sample.setMetaDouble("nc_sumw",sumw)


def getListOfTreeNames(sample, treePrefix = ""):
	"""
	Grab a list of tree names in the first file of the sample
	"""

	f = ROOT.TFile.Open( sample.fileName(0)  )
	listOfTrees = [key.GetName() for key in f.GetListOfKeys() if key.ReadObj().InheritsFrom("TTree") and treePrefix in key.GetName() ]
        listOfTrees = set(listOfTrees) # keep only one name for different revision number

	return listOfTrees


def setBranchStatus(tree,branchSelectionString,status=0):
    """This is used by _copyTreeSubset() to turn on/off branches"""
    for branchToModify in branchSelectionString.split(","):
        print("Setting branch status to %d for %s"%(status,branchToModify)  )
        tree.SetBranchStatus(branchToModify,status)
    return tree


if __name__ == "__main__":
	main()

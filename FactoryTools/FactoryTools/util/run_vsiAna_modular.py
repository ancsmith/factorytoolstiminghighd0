#!/usr/bin/env python

import ROOT, logging, collections, commonOptions
# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False

parser = commonOptions.parseCommonOptions()

#you can add additional options here if you want
#parser.add_option('--verbosity', help   = "Run all algs at the selected verbosity.",choices=("info", "warning","error", "debug", "verbose"), default="error")

(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)


######################################################################
##


logging.info("creating algorithms")

outputFilename = "VsiAnaModular"
output = ROOT.EL.OutputStream(outputFilename);
job.outputAdd(output)

#here we add the algorithms we want to run over
import collections
algsToRun = collections.OrderedDict()

from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
vsialg_mod   = ROOT.VsiAnaModular()


def setupVsiAlg ( vsiana ):
    vsiana.setMsgLevel( 3 )
    vsiana.prop_probeTruth            = "HadInt"
    vsiana.prop_mode                  = ROOT.VsiAnaExample.r21
    vsiana.prop_chi2Cut               = 9999.
    vsiana.prop_hitPatternReq         = 0
    vsiana.prop_doDropAssociated      = False

setupVsiAlg( vsialg_mod )
    
vsialg_mod.prop_outputName       = "svTree_mod"
vsialg_mod.prop_outputFileName   = outputFilename
vsialg_mod.prop_do_eventInfo = True
vsialg_mod.prop_do_truth = True
vsialg_mod.prop_do_IDTracksPrescaled = True
vsialg_mod.prop_do_IDTracksSelected = True
vsialg_mod.prop_do_primaryVertices = True
vsialg_mod.prop_do_vsiVertices = False

algsToRun["vsialg_mod"]  = vsialg_mod

##
######################################################################


commonOptions.addAlgsFromDict(job , algsToRun , options.verbosity)

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , gridUser = options.gridUser , gridTag = options.gridTag, groupRole = options.groupRole, nEvents = options.nevents)


import ROOT
import logging
import shutil
import os


def discover(sh, search_directories, pattern="*trees*"):

	# scan for datasets in the given directories
	for directory in search_directories:
	    ROOT.SH.ScanDir().samplePattern(pattern).scan(sh, directory)


	logging.info("%d different datasets found scanning all directories", len(sh))

	return sh


def discoverFileList(sh, fileNameList, sampleName):
        with open('tempFileList.txt', mode='wt') as myfile:
            myfile.write('\n'.join(fileNameList))
        ROOT.SH.readFileList(sh, sampleName, "tempFileList.txt")


def addTags(sh_all,tagConfigFunction):
	for sample in sh_all:

		sample_name = sample.getMetaString("sample_name")
		# Interprets first chunk of text that can cast into an int as the DSID
		dsid = "0"
		for ichunk,chunk in enumerate(sample.getMetaString("sample_name").split(".") ):
			if chunk.isdigit() or "period" in chunk:
				dsid = chunk
				short_name = sample.getMetaString("sample_name").split(".")[ichunk+1]
				break
		sample.setMetaString( "dsid", dsid )
		if "data" in sample_name:
			sample.setMetaString( "dsid", "Data_"+dsid )

		if "data15" in sample_name:
			sample.setMetaString( "dsid", "Data2015_"+dsid )
		elif "data16" in sample_name:
			sample.setMetaString( "dsid", "Data2016_"+dsid )

		sample.setMetaString( "short_name" , sample.getMetaString("sample_name") )

		short_name = sample.getMetaString( "short_name" )

		print(short_name)

		## Data Labelling
		if "physics_" in short_name or "data" in short_name:
			sample.addTag("data")
			if "data15" in short_name:
				sample.addTag("data15")
			if "data16" in short_name:
				sample.addTag("data16")
			if "data17" in short_name:
				sample.addTag("data17")
			if "data18" in short_name:
				sample.addTag("data18")
			continue

		tagConfigFunction(sample)


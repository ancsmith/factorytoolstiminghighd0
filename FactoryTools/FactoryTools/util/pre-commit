#!/bin/sh
#
# An example hook script to verify what is about to be committed.
# Called by "git commit" with no arguments.  The hook should
# exit with non-zero status after issuing an appropriate message if
# it wants to stop the commit.
#
# To enable this hook, rename this file to "pre-commit".

if git status | grep --quiet README ; then
	echo ">>> Running pre-commit hook to create INSTALL.sh "
	echo "### This INSTALL.sh is auto-generated upon commit. Don\'t touch manually!\n" > INSTALL.sh
	grep "\#\!" README.md >> INSTALL.sh
	echo ">>> Adding INSTALL.sh to commit"
	git add INSTALL.sh
else
	echo ">>> No changes to README detected. Don't need to update INSTALL.sh."
fi

# Checking if I need to update the AR used in the CI

function ver { printf "%03d%03d%03d%03d" $(echo "$1" | tr '.' ' '); }

UNFORMATTED_SETUP_VERSION=$(grep ^ANALYSIS_BASE_VERSION util/setup.sh | cut -d'=' -f2)
SETUP_VERSION=$(ver ${UNFORMATTED_SETUP_VERSION})
CI_VERSION=$(ver $(grep "atlas/analysisbase:" .gitlab-ci.yml | cut -d':' -f3) )
DOCKER_VERSION=$(ver $(grep "atlas/analysisbase:" Dockerfile | cut -d':' -f2) )
if [ ${SETUP_VERSION} -ne ${CI_VERSION} ]; then
	echo ">>> Mismatch between ARs in util/setup.sh and CI config."
	echo ">>> Changing CI to use AR ${UNFORMATTED_SETUP_VERSION}"
	sed -i "s/.*atlas\/analysisbase:.*/  image: atlas\/analysisbase:${UNFORMATTED_SETUP_VERSION}/" .gitlab-ci.yml
	git add .gitlab-ci.yml
fi

if [ ${SETUP_VERSION} -ne ${DOCKER_VERSION} ]; then
	echo ">>> Mismatch between ARs in util/setup.sh and Dockerfile config."
	echo ">>> Changing Dockerfile to use AR ${UNFORMATTED_SETUP_VERSION}"
	sed -i "s/.*atlas\/analysisbase:.*/FROM atlas\/analysisbase:${UNFORMATTED_SETUP_VERSION}/" Dockerfile
	git add Dockerfile
fi
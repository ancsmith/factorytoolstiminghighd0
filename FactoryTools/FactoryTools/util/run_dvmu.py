#!/usr/bin/env python

import ROOT, logging, collections, commonOptions
# Workaround to fix threadlock issues with GUI
ROOT.PyConfig.StartGuiThread = False

parser = commonOptions.parseCommonOptions()

#you can add additional options here if you want
#parser.add_option('--verbosity', help   = "Run all algs at the selected verbosity.",choices=("info", "warning","error", "debug", "verbose"), default="error")

(options, args) = parser.parse_args()

job = commonOptions.initializeRunScript(options, args)


######################################################################
##


logging.info(">>> Creating algorithms")

outputFilename = "trees"
output = ROOT.EL.OutputStream(outputFilename);

#here we add the algorithms we want to run over
import collections
algsToRun = collections.OrderedDict()

algsToRun["basicEventSelection"]       = ROOT.BasicEventSelection()
commonOptions.configxAODAnaHelperAlg(algsToRun["basicEventSelection"] )
setattr(algsToRun["basicEventSelection"], "m_triggerSelection", ".+" )
setattr(algsToRun["basicEventSelection"], "m_useMetaData"  , False )
setattr(algsToRun["basicEventSelection"], "m_doPUreweighting" , False )
setattr(algsToRun["basicEventSelection"], "m_PRWFileNames" , "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/rpvll_DV.prw.root")
setattr(algsToRun["basicEventSelection"], "m_applyGRLCut" , True)


algsToRun["calibrateST"]               = ROOT.CalibrateST()
algsToRun["calibrateST"].SUSYToolsConfigFileName = "$FactoryToolsWrapper_DIR/data/FactoryTools/SUSYTools_dvmu.conf"
algsToRun["calibrateST"].doPRW                    = algsToRun["basicEventSelection"].m_doPUreweighting;
algsToRun["calibrateST"].PRWConfigFileNames       = algsToRun["basicEventSelection"].m_PRWFileNames
algsToRun["calibrateST"].PRWLumiCalcFileNames     = algsToRun["basicEventSelection"].m_lumiCalcFileNames
setattr(algsToRun["calibrateST"], "useLLPMuons", 1)

algsToRun["storeDVObjects"]               = ROOT.StoreDVObjects()
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_FileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/MaterialMap_v3.2_Inner.root")
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_HistName", "FinalMap_inner")
setattr(algsToRun["storeDVObjects"], "materialMap_Inner_MatrixName", "FoldingInfo")
setattr(algsToRun["storeDVObjects"], "materialMap_Outer_FileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/MaterialMap_v3_Outer.root")
setattr(algsToRun["storeDVObjects"], "materialMap_Outer_HistName", "matmap_outer")

# Main selection and sorting algorithm
algsToRun["selectDV"]        = ROOT.SelectDVEvents()
setattr(algsToRun["selectDV"], "m_selectionString", "muon")

# Adds information about displaced muons
# Must run after selectDV and before calculateRegionVars
algsToRun["storeMuonDVObjects"]               = ROOT.StoreMuonDVObjects()
setattr(algsToRun["storeMuonDVObjects"], "segmentMapFileName", "$FactoryToolsWrapper_DIR/data/FactoryTools/DV/segmentMap2D_Run2_v4.root")
setattr(algsToRun["storeMuonDVObjects"], "InnerMapHistName", "BI_mask")
setattr(algsToRun["storeMuonDVObjects"], "MiddleMapHistName", "BM_mask")
setattr(algsToRun["storeMuonDVObjects"], "OuterMapHistName", "BO_mask")
setattr(algsToRun["storeMuonDVObjects"], "m_sumEta", 0.05)
setattr(algsToRun["storeMuonDVObjects"], "m_deltaPhi", 0.22)
setattr(algsToRun["storeMuonDVObjects"], "m_minEta", 2.5)
setattr(algsToRun["storeMuonDVObjects"], "m_minPt", 25.)
setattr(algsToRun["storeMuonDVObjects"], "m_minD0", 1.5)
setattr(algsToRun["storeMuonDVObjects"], "m_maxD0", 300)
setattr(algsToRun["storeMuonDVObjects"], "m_trackIso", 0.06)
setattr(algsToRun["storeMuonDVObjects"], "m_caloIso", 0.06)

# These are the calculators that calculate various derived quantities
algsToRun["calculateRegionVars"]                      = ROOT.CalculateRegionVars()
algsToRun["calculateRegionVars"].m_calculator         = ROOT.RegionVarCalculator_dv()

# The ntuple writer will automatically write-out the trig decisions for the triggers listed below
algsToRun["calculateRegionVars"].triggerBranches      = ROOT.std.vector(ROOT.std.string)() #stupid root... can't initialize the vector here...
algsToRun["calculateRegionVars"].triggerBranches      += ["HLT_mu60_0eta105_msonly","HLT_xe100_mht_L1XE50", "HLT_xe110_mht_L1XE50","HLT_xe110_mht_L1XE50_AND_xe70_L1XE50","HLT_xe110_pufit_L1XE60"] # list triggers here e.g. ["HLT_xe70","HLT_xe100"]

# The ntuple writer will write out additional information about the following objects
algsToRun["calculateRegionVars"].listOfDetailedObjects      = ROOT.std.vector(ROOT.std.string)() #stupid root... can't initialize the vector here...
algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["dvTracks","muons","msTracks","msSegments"]
#algsToRun["calculateRegionVars"].listOfDetailedObjects      += ["dvTracks","muons","msTracks","idTracks","msSegments"] # list triggers here e.g. ["HLT_xe70","HLT_xe100"]


# These correspond to writing out the various trees used in the analysis
#for regionName in ["SRMET","SRLEP"]:
for regionName in ["SRDV"]:
    tmpWriteOutputNtuple                       = ROOT.WriteOutputNtuple()
    tmpWriteOutputNtuple.outputName            = outputFilename
    tmpWriteOutputNtuple.regionName            = regionName
    # tmpWriteOutputNtuple.systVar            = 0
    algsToRun["writeOutputNtuple_"+regionName] = tmpWriteOutputNtuple



##
######################################################################




if options.doSystematics :
	algsToRun = commonOptions.doSystematics(algsToRun,fullChainOnWeightSysts = 0, excludeStrings = ["JET_Rtrk_","TAUS_"])

job.outputAdd(output);
commonOptions.addAlgsFromDict(job , algsToRun , options)

commonOptions.overwriteSubmitDir(options.submitDir , options.doOverwrite)
commonOptions.submitJob         ( job , options.driver , options.submitDir , gridUser = options.gridUser , gridTag = options.gridTag, groupRole = options.groupRole, nEvents = options.nevents)

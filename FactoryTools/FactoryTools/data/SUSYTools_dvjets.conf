##################################################
# SUSYTools configuration file
##################################################
EleBaseline.Pt: 10000.
EleBaseline.Eta: 2.47
EleBaseline.Id: LooseAndBLayerLLH
EleBaseline.CrackVeto: false
#
Ele.Et: 25000.
Ele.Eta: 2.47
Ele.CrackVeto: false
Ele.Iso: Gradient
Ele.IsoHighPt: FCHighPtCaloOnly # tight iso required for electrons pt > 200 GeV
Ele.Id: TightLLH
Ele.d0sig: 5.
Ele.z0: 0.5
# ChargeIDSelector WP
Ele.CFT: None
#
MuonBaseline.Pt: 10000.
MuonBaseline.Eta: 2.7
MuonBaseline.Id: 1 # Medium
MuonBaseline.z0: -99
#
Muon.Pt: 25000.
Muon.Eta: 2.7
Muon.Id: 1 # Medium
#Muon.Iso: FCLoose


##### WARNING. THIS IS TO FIX THE LACK OF A DECORATOR IN OUR DAOD_RPVLL THAT IS NEEDED IN 21.2.56. NEED TO RUN ON DERIVATIONS TO GET THIS TO WORK...
SigMu.RequireIso: false
#
Muon.d0sig: 3.
Muon.z0: 0.5
#
MuonCosmic.z0: 1.
MuonCosmic.d0: 0.2
#
BadMuon.qoverp: 0.2
#
PhotonBaseline.Pt: 25000.
PhotonBaseline.Eta: 2.37
PhotonBaseline.Id: Tight
#
Photon.Pt: 130000.
Photon.Eta: 2.37
Photon.Id: Tight
Photon.Iso: FixedCutTight
#
Tau.Pt: 20000.
Tau.Eta: 2.5
Tau.Id: Medium
#Tau.DoTruthMatching: false
#
Jet.Pt: 20000.
Jet.Eta: 2.8
Jet.InputType: 9 # EMTopo 1, PFlow: 9
Jet.JvtWP: Medium
#Jet.JvtPtMax: 120.0e3
Jet.UncertConfig: rel21/Summer2018/R4_StrongReduction_Scenario1_SimpleJER.config
#
FwdJet.doJVT: false
FwdJet.JvtEtaMin: 2.5
FwdJet.JvtPtMax: 50e3
FwdJet.JvtWP: Loose
#
Jet.LargeRcollection: AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets # set to None to turn this off
Jet.LargeRuncConfig: rel21/Spring2019/R10_GlobalReduction.config
#
Jet.WtaggerConfig: SmoothedInclWTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency50_SUSYOpt_MC16_20210129.dat # set to None to turn this off
Jet.ZtaggerConfig: SmoothedInclZTagger_AntiKt10LCTopoTrimmed_FixedSignalEfficiency50_SUSYOpt_MC16_20210129.dat # set to None to turn this off
Jet.ToptaggerConfig: JSSDNNTagger_AntiKt10LCTopoTrimmed_TopQuarkInclusive_MC16d_20190405_80Eff.dat # set to None to turn this off
#
Jet.JMSCalib: None # Non, Extrap, Frozen
#
#This is the derivation level, bad jet cut we use, have option of LooseBad, VeryLooseBad, LooseBadLLP, VeryLooseBadLLP, and SuperLooseBadLLP.
#Currently can only use one flag at a time, user should choose own flag below.
BadJet.Cut: SuperLooseBadLLP
#
#master switch for btagging use in ST. If false, btagging is not used neither for jets decorations nor for OR (regardless of the options below)
Btag.enable: true
#
Btag.Tagger: DL1 # MV2c10, DL1mu, DL1rnn, MV2c10mu, MV2c10rnn, MC2cl100_MV2c100
Btag.WP: FixedCutBEff_77
Btag.MinPt: 20000.
Btag.TimeStamp: 201903
#
TrackJet.Coll: AntiKtVR30Rmax4Rmin02TrackJets # AntiKt2PV0TrackJets
TrackJet.Pt: 20000.
TrackJet.Eta: 2.8
BtagTrkJet.Tagger: MV2c10
BtagTrkJet.WP: FixedCutBEff_77
BtagTrkJet.MinPt: 10000.
#
# set the -999. to positive number to override default
OR.DoBoostedElectron: true
OR.BoostedElectronC1: -999.
OR.BoostedElectronC2: -999.
OR.BoostedElectronMaxConeSize: -999.
OR.DoBoostedMuon: true
OR.BoostedMuonC1: -999.
OR.BoostedMuonC2: -999.
OR.BoostedMuonMaxConeSize: -999.
OR.DoMuonJetGhostAssociation: true
OR.DoTau: false
OR.DoPhoton: false
OR.Bjet: true
OR.ElBjet: true
OR.MuBjet: true
OR.TauBjet: false
OR.MuJetApplyRelPt: false
OR.MuJetPtRatio: -999.
OR.MuJetTrkPtRatio: -999.
OR.RemoveCaloMuons: true
OR.MuJetInnerDR: 0.0001
OR.BtagWP: FixedCutBEff_85
#
OR.DoFatJets: false
OR.EleFatJetDR: -999.
OR.JetFatJetDR: -999.
#OR.InputLabel: selected
#
SigLep.RequireIso: true
#
MET.EleTerm: RefEle
MET.GammaTerm: RefGamma
MET.TauTerm: RefTau
MET.JetTerm: RefJet
MET.MuonTerm: Muons
MET.OutputTerm: Final
MET.JetSelection: Tight # Loose, Tight, Tighter, Tenacious
MET.RemoveOverlappingCaloTaggedMuons: true
MET.DoRemoveMuonJets: true
MET.UseGhostMuons: false
MET.DoMuonEloss: false
#
# Trigger SFs configuration
# Ele.TriggerSFStringSingle: SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2017_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0
Trigger.UpstreamMatching: true
Trigger.MatchingPrefix: TrigMatch_
#
# actual Mu files have to be set in SUSYTools
PRW.ActualMu2017File: GoodRunsLists/data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root
PRW.ActualMu2018File: GoodRunsLists/data18_13TeV/20181111/purw.actualMu.root
#
StrictConfigCheck: true

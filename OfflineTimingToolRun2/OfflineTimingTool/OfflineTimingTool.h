//________________________________
//
// Offline Precision Timing Tool
//
// Original tool, 2015 developments:
// 	Author:	Ryne Carbone
// 	Date  : Jan 2016
// 	Email : ryne.carbone@cern.ch
//
// 2016 developments:
// 	Author: Jue Chen
// 	Email : jue@cern.ch
//
// Full Run 2 tool and corrections:
// 	Author: Devin Mahon
// 	Email : devin.mahon@cern.ch 
//________________________________
//

#ifndef OFFLINETIMINGTOOL_H
#define OFFLINETIMINGTOOL_H

// Event Loop Includes
#include <EventLoop/StatusCode.h>

#include <tuple>
// PathResolver
#include "PathResolver/PathResolver.h"

// General Includes
#include <iostream>
#include <fstream>
#include <numeric>
#include "TFile.h"
#include "TMath.h"
#include "TRandom3.h"
#include "boost/algorithm/string.hpp"
#include <TGraph.h>
#include <map>

using namespace std;  

class OfflineTimingTool{

  private:

    // In LArHelp.cxx, HW information
    int getChannelNo(unsigned long int);
    int getSlotNo   (unsigned long int);
    int getFebNo    (unsigned long int);
    int getFtNo     (unsigned long int);
  
    const double GeV = 1000.; //! convert MeV to GeV
    bool m_debug;             //! more verbose printout  

    // Vectors to store corrections
    // Key: IOV# -- Value: RunNumber
    std::vector<int> 		      iovNumberList; //! 
    std::vector<int>                  runNumberList; //!
    std::vector<int>                  channelList_for_subslot; //!
    std::vector<int>                  bad_channelList15; //!
    std::vector<int>                  bad_channelList16; //!
    std::vector<int>                  bad_channelList17; //!
    std::vector<int>                  bad_channelList18; //!
    // Key: IOV# -- Value: Vector FT indicies, with vector of corr by run#
    std::map< int, std::vector< std::vector<std::string> > > p0CorrH; //!
    std::map< int, std::vector< std::vector<std::string> > > p0CorrM; //!
    // Key: IOV# -- Value: Vector of FEB corr
    std::map< int, std::vector< std::string > >              p1CorrH; //!
    std::map< int, std::vector< std::string > >              p1CorrM; //!
    // Key: IOV# -- Value: Vector of Channel corr
    std::map< int, std::vector< std::string > >              p2CorrH; //!
    std::map< int, std::vector< std::string > >              p2CorrM; //!
    // Key: IOV# -- Value: Vector Slot indicies, with vector of fit params
    std::map< int, std::vector< std::vector<std::string> > > p3CorrH; //!
    std::map< int, std::vector< std::vector<std::string> > > p3CorrM; //!
    std::map< int, std::vector< std::vector<std::string> > > p4CorrH; //!
    std::map< int, std::vector< std::vector<std::string> > > p4CorrM; //!
    std::map< int, std::vector< std::vector<std::string> > > p5CorrH; //!
    std::map< int, std::vector< std::vector<std::string> > > p5CorrM; //!
    // Key: IOV# -- Value: Vector of Channel corr repeated
    std::map< int, std::vector< std::string > >              p6CorrH; //!
    std::map< int, std::vector< std::string > >              p6CorrM; //!
    // Key: IOV# -- Value: Vector Slot indicies, with vector of fit params
    std::map< int, std::vector< std::vector<std::string> > > pPhotonCorrH; //!
    std::map< int, std::vector< std::vector<std::string> > > pPhotonCorrM; //!

    // For smearing MC times
    double m_sig_beamSpread = 0.180; //!
    double m_sig_uncorrEMEC = 0.025; //!
    std::vector< TGraph* > smearUncorrFitH; //!
    std::vector< std::vector<std::string> > smearUncorrFitM; //!

  public:
    // Constructor
    OfflineTimingTool(bool debug=false);
    // Initialize/Load corrections
    EL::StatusCode initialize();

    // Variables need to compute offline correction
    struct caloObject_t{
      bool		m_isPhoton;       // Is the object a photon (receives extra correction if so)
      unsigned long int m_onlId;          // LAr Online Id 
      unsigned int      m_rn;             // Run number    
      unsigned int      m_rn_ind;         // Run index in IOV
      int               m_iov;            // IOV number
      int           	m_year;           // Year
      int               m_gain;           // Max Ecell Gain
      float             m_energy;         // Max Ecell Energy(store in GeV!)
      float             m_time;           // Max Ecell Time
      float             m_x;              // Max Ecell X
      float             m_y;              // Max Ecell Y
      float             m_z;              // Max Ecell Z
      float             m_clusterE;       // Energy from calocluster
      float             m_etas2;          // Etas2 from calocluster
      float             m_phis2;          // Phis2 from calocluster
      float             m_f1;             // Fraction of energy in layer 1
      float             m_f3;             // Fraction of energy in layer 3
      float             m_PV_z;           // Primary Vertex Z
      double            m_dphi;           // Dif in phi from barycenter of calClust & cell
      double            m_deta;           // Dif in eta from barycenter of calClust & cell
      double            m_corrected_time; // Corrected time
      bool 	        valid; 		  // flag the correction is valid
    };

    typedef struct caloObject_t caloObject_t;

    // Set the local variables for calo object
    template <class T> 
    OfflineTimingTool::caloObject_t setCaloObject(T calObj, bool isPhoton, unsigned int rn, float PV_z){ 
      // calo Object to return
      caloObject_t return_obj;
      return_obj.m_isPhoton = isPhoton;
      return_obj.m_rn       = rn;
      return_obj.m_onlId    = calObj.template auxdata<unsigned long int>("maxEcell_onlId");
      return_obj.m_onlId    = return_obj.m_onlId >> 32; //Is stored as 64bit, last 32 bits are zeros
      return_obj.m_gain     = calObj.template auxdata<int>("maxEcell_gain");
      return_obj.m_energy   = calObj.template auxdata<float>("maxEcell_energy")/GeV;
      return_obj.m_time     = calObj.template auxdata<float>("maxEcell_time");
      return_obj.m_x        = calObj.template auxdata<float>("maxEcell_x");
      return_obj.m_y        = calObj.template auxdata<float>("maxEcell_y");
      return_obj.m_z        = calObj.template auxdata<float>("maxEcell_z");
      return_obj.m_f1       = calObj.template auxdata<float>("f1");
      return_obj.m_f3       = calObj.template auxdata<float>("f3");
      // Calo Cluster info
      return_obj.m_etas2    = calObj.template caloCluster()->etaBE(2); //auxdata<float>("caloCluster_etas2");
      return_obj.m_phis2    = calObj.template caloCluster()->phiBE(2); //auxdata<float>("caloCluster_phis2");
      return_obj.m_clusterE = calObj.template caloCluster()->e()/GeV; //auxdata<float>("caloCluster_e")/GeV;
      // PV
      return_obj.m_PV_z     = PV_z;
      // Time to keep track of corrections
      return_obj.m_corrected_time = calObj.template auxdata<float>("maxEcell_time");
      return_obj.valid      = true;
      return return_obj;
    }

    // Print out information (for debugging)
    void dumpInfo              (caloObject_t &calObj);

    // Primary Vertex Z / Angular Cell Position
    EL::StatusCode setIOV      (caloObject_t &calObj);
    EL::StatusCode setdPhidEta (caloObject_t &calObj);

    // Apply each correction

    double       applyCorrections(caloObject_t &calObj);
    EL::StatusCode Set_valid(caloObject_t &calObj);
    EL::StatusCode tofCorrection (caloObject_t &calObj); // Time of Flight
    EL::StatusCode applyPass0Corr(caloObject_t &calObj); // Run by Run FT time
    EL::StatusCode applyPass1Corr(caloObject_t &calObj); // Avg FEB time
    EL::StatusCode applyPass2Corr(caloObject_t &calObj); // Avg Ch time
    EL::StatusCode applyPass3Corr(caloObject_t &calObj); // Avg En correction
    EL::StatusCode applyPass4Corr(caloObject_t &calObj); // dphi/deta correction
    EL::StatusCode applyPass5Corr(caloObject_t &calObj); // f1/f3 correction
    EL::StatusCode applyPass6Corr(caloObject_t &calObj); // Avg Ch time
    EL::StatusCode applyPhotonCorr(caloObject_t &calObj);// Additional photon correction
    
    // Apply smearing
    double applySmearing(caloObject_t &calObj, double &t_coll, bool correlate, int mode);

    // Calculate offline correction
    template <class T> 
	    std::tuple<bool, double>getCorrectedTime(T calObj, bool isPhoton, unsigned int rn, float PV_z){
		    // Set the local variables for this calo object
		    caloObject_t curr_obj = setCaloObject<T>(calObj, isPhoton, rn, PV_z);
		    // Set the IOV
		    if( setIOV( curr_obj ) != EL::StatusCode::SUCCESS ){
			    if( m_debug ) Info("OfflineTimingTool::getCorrectedTime", "Unable to set IOV, cannot get correction, returning time = -99999." );
                            curr_obj.valid = false;
                            curr_obj.m_time = -99999;
			    return std::make_tuple(curr_obj.valid, curr_obj.m_time);
		    }      
		    // Set dPhi/dEta
		    EL::StatusCode succeed = setdPhidEta( curr_obj );
                    if (!succeed && m_debug) {Info("OfflineTimingTool::setdPhidEta","Unable to set dPhidEta..." ); }
		    double corrected_time = applyCorrections( curr_obj );
                    if ( m_debug && curr_obj.valid) Info("OfflineTimingTool::getCorrectedTime", "Corrections computation successfully completed." );
		    return std::make_tuple(curr_obj.valid, corrected_time);
	    }

	    
	    // Additional function for use with ntuples (user sets rn and PV_z when creating caloObject)
       	    std::tuple<bool, double> getCorrectedTime(caloObject_t & curr_obj){
                    // Set the IOV
		    if( setIOV( curr_obj ) != EL::StatusCode::SUCCESS ){
			    if( m_debug ) Info("OfflineTimingTool::getCorrectedTime", "Unable to set IOV, cannot get correction, returning time = -99999." );
                            curr_obj.valid = false;
                            curr_obj.m_time = -99999;
			    return std::make_tuple(curr_obj.valid, curr_obj.m_time);
		    }      
		    // Set dPhi/dEta
		    EL::StatusCode succeed = setdPhidEta( curr_obj );
                    if (!succeed && m_debug) {Info("OfflineTimingTool::setdPhidEta","Unable to set dPhidEta..." ); }
		    // Apply the corrections
		    double corrected_time = applyCorrections( curr_obj );
                    if ( m_debug && curr_obj.valid) Info("OfflineTimingTool::getCorrectedTime", "Corrections computation successfully completed." );
		    return std::make_tuple(curr_obj.valid, corrected_time);
	    }

    // Calculate MC smeared time
    // set correlate to true if using previous t_coll
    template <class T>
	    std::tuple<bool, double> getSmearedTime(T calObj, float PV_z, double &t_coll, bool correlate, int mode){
		    // Set local variables for this calo object (rn number is not needed, set to 0)
		    caloObject_t curr_obj = setCaloObject<T>(calObj, true, 0, PV_z);
		    // Calculate time of flight correction
		    if( tofCorrection( curr_obj ) != EL::StatusCode::SUCCESS ){
			    if( m_debug) Info("OfflineTimingtool::getSmearedTime", "Unable to calculate TOF correction, returning raw time!" );
			    curr_obj.valid = false;
			    return std::make_tuple(curr_obj.valid, curr_obj.m_time);
		    }
		    // Apply smearing
		    double t_smear = applySmearing(curr_obj, t_coll, correlate, mode) ;
		    return std::make_tuple(curr_obj.valid, t_smear);
	    }

            // Additional function for use with ntuples
	    std::tuple<bool, double> getSmearedTime(caloObject_t & curr_obj, double &t_coll, bool correlate, int mode){
		    // Calculate time of flight correction
		    if( tofCorrection( curr_obj ) != EL::StatusCode::SUCCESS ){
			    if( m_debug) Info("OfflineTimingtool::getSmearedTime", "Unable to calculate TOF correction, returning raw time!" );
                            curr_obj.valid = false;
			    return std::make_tuple(curr_obj.valid, curr_obj.m_time);
		    }
		    // Apply smearing
		    double t_smear = applySmearing(curr_obj, t_coll, correlate, mode);
		    return std::make_tuple(curr_obj.valid, t_smear);
	    }

    // Destructor
    ~OfflineTimingTool(){};

};

#endif
